//
//  ArticleCategoriesController.swift
//  PlantApp
//
//  Created by Nayyer Ali on 11/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit

struct ArticleCategoriesData {
    var name: String
    var image: UIImage
}

class ArticleCategoriesController: UIViewController {
    
    //MARK: Public Variables
    
    //MARK: Private Variables
    
    var articleCategoriesData = [ArticleCategoriesData]()
    
    //MARK: Outlets
    
    @IBOutlet weak var tableViewOut: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewOut.delegate = self
        tableViewOut.dataSource = self
        getData()
    }
        
        func getData(){
            let item1 = ArticleCategoriesData(name: "Sunflower", image: UIImage(named: "13")!)
            let item2 = ArticleCategoriesData(name: "Ross", image: UIImage(named: "14")!)
            let item3 = ArticleCategoriesData(name: "Lily", image: UIImage(named: "15")!)
            let item4 = ArticleCategoriesData(name: "Vanilla", image: UIImage(named: "16")!)
            let item5 = ArticleCategoriesData(name: "Almond", image: UIImage(named: "17")!)
            let item6 = ArticleCategoriesData(name: "Pistacho", image: UIImage(named: "18")!)
            articleCategoriesData.append(item1)
            articleCategoriesData.append(item2)
            articleCategoriesData.append(item3)
            articleCategoriesData.append(item4)
            articleCategoriesData.append(item5)
            articleCategoriesData.append(item6)
        }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ArticleCategoriesController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articleCategoriesData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleCategoryTableViewCell") as! ArticleCategoryTableViewCell
        cell.nameOut.text = articleCategoriesData[indexPath.row].name
        cell.imageOut.image = articleCategoriesData[indexPath.row].image
        return cell
    }
}
