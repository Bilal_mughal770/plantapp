//
//  DiscoverController.swift
//  PlantApp
//
//  Created by Nayyer Ali on 11/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit
import ObjectMapper
struct Datas {
    var title: String
    var image: UIImage
    var type: String
    var price: String
}

class DiscoverController: UIViewController {
    
    //MARK: Public Variables
    
    //MARK: Private Variables
    
    var data = [Datas]()

    var arrayDiscoverMenu = [DiscoverMenuModel]()
    
    //MARK: Outlets
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tableViewOut: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewOut.delegate = self
        tableViewOut.dataSource = self
//        segmentedControl.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 2))
        getData()
        self.getDiscoverMenu()
        self.getTaxes()
    }
    
    func getData(){
        let item1 = Datas(title: "Sunflower", image: UIImage(named: "1")!, type: "Outdoor", price: "15$")
        let item2 = Datas(title: "Ross", image: UIImage(named: "2")!, type: "Indoor", price: "17$")
        let item3 = Datas(title: "Lily", image: UIImage(named: "3")!, type: "Outdoor", price: "20$")
        let item4 = Datas(title: "Vanilla", image: UIImage(named: "4")!, type: "Indoor", price: "25$")
        let item5 = Datas(title: "Berry", image: UIImage(named: "5")!, type: "Outdoor", price: "80$")
        data.append(item1)
        data.append(item2)
        data.append(item3)
        data.append(item4)
        data.append(item5)
    }
}

extension DiscoverController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiscoverTableViewCell") as! DiscoverTableViewCell
        cell.imageOut.image = data[indexPath.row].image
        cell.titleOut.text = data[indexPath.row].title
        cell.typeOut.text = data[indexPath.row].type
        cell.priceOut.text = data[indexPath.row].price
        return cell
    }
}


//MARK:- SERVICE

extension DiscoverController{
    private func getDiscoverMenu(){
        APIManager.sharedInstance.usersAPIManager.getDiscoverMenu { (responseObject) in
            print(responseObject)
            let discoverMenu = Mapper<DiscoverMenuModel>().mapArray(JSONObject: responseObject) ?? [DiscoverMenuModel]()
//            let segmentTitle1 = discoverMenu[0].menuItem
//            self.segmentedControl.setTitle(segmentTitle1, forSegmentAt: 0)
//            let segmentTitle2 = discoverMenu[1].menuItem
//            self.segmentedControl.setTitle(segmentTitle2, forSegmentAt: 1)
//            let segmentTitle3 = discoverMenu[2].menuItem
//            self.segmentedControl.setTitle(segmentTitle3, forSegmentAt: 2)


            print(discoverMenu)
        } failure: { (error) in
            print(error)
        }
    }
    
    
    private func getTaxes() {
        
        APIManager.sharedInstance.usersAPIManager.getTaxes { (responseObject) in
            print(responseObject)
        } failure: { (error) in
            print("response Taxes : - ",error.localizedDescription)
        }

    }
}
