//
//  DiscoverTableViewCell.swift
//  PlantApp
//
//  Created by Nayyer Ali on 23/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit

class DiscoverTableViewCell: UITableViewCell {
    
    //MARK: Outlets
    
    @IBOutlet weak var imageOut: CustomImage!
    @IBOutlet weak var titleOut: UILabel!
    @IBOutlet weak var typeOut: UILabel!
    @IBOutlet weak var priceOut: UILabel!
    @IBOutlet weak var favouriteIcon: UIButton!
    @IBOutlet weak var addToCartBtnOut: CustomButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
