//
//  TrainingCollectionViewCell.swift
//  PlantApp
//
//  Created by Nayyer Ali on 25/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit

class TrainingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var subtitleOut: UILabel!
    @IBOutlet weak var titleOut: UILabel!
    @IBOutlet weak var imageOut: CustomImage!
}
