//
//  ForYouCollectionViewCell.swift
//  PlantApp
//
//  Created by Nayyer Ali on 25/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit

class ForYouCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var headingOut: UILabel!
    @IBOutlet weak var imageOut: CustomImage!
}
