//
//  VideosController.swift
//  PlantApp
//
//  Created by Nayyer Ali on 11/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit
import ObjectMapper
struct NewVideosData {
    var image: UIImage
}

struct TrainingData {
    var title: String
    var subtitle: String
    var image: UIImage
}

struct ForYouData {
    var heading: String
    var image: UIImage
}

class VideosController: UIViewController {
    
    //MARK: Public Variables
    
    //MARK: Private Variables
    
    private var newVideosData = [NewVideosData]()
    private var trainingData = [TrainingData]()
    private var forYouData = [ForYouData]()
    private var data = [String:Any]()
    
    //MARK: Outlets
    
    @IBOutlet weak var newVideosCollectionViewOut: UICollectionView!
    @IBOutlet weak var trainingCollectionViewOut: UICollectionView!
    @IBOutlet weak var forYouCollectionViewOut: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newVideosCollectionViewOut.delegate = self
        newVideosCollectionViewOut.dataSource = self
        trainingCollectionViewOut.delegate = self
        trainingCollectionViewOut.dataSource = self
        forYouCollectionViewOut.delegate = self
        forYouCollectionViewOut.dataSource = self
        getNewVideosData()
        getTrainingData()
        getForYouData()
        getSingleVideoDetail()
//        getVideoListForYou()
//        getTrainingVideoList()
//        getNewVideoList()
//        getVideoList()
    }
    
    func getVideoListForYou (){
        ServerCommunication.shared.videosListNew { (status, message, data) in
            if status{
                self.data = data!
                self.newVideosCollectionViewOut.reloadData()
                print("Response aya hay")
            }else {
                print("getVideoListForYou Bhand aya")
            }
            
        }
    }
    
    func getNewVideosData(){
        let item1 = NewVideosData(image:UIImage(named: "25")!)
        let item2 = NewVideosData(image:UIImage(named: "24")!)
        let item3 = NewVideosData(image:UIImage(named: "23")!)
        newVideosData.append(item1)
        newVideosData.append(item2)
        newVideosData.append(item3)
    }
    
    func getTrainingData(){
        let item1 = TrainingData(title: "John Trevolta", subtitle: "Disco step guy",image:UIImage(named: "Image-3")!)
        let item2 = TrainingData(title: "Samule L. Jackson", subtitle: "Aka. Nick Fury",image:UIImage(named: "Image-4")!)
        let item3 = TrainingData(title: "Chrish Hemsworth", subtitle: "The most suited guy for thor",image:UIImage(named: "Image-5")!)
        trainingData.append(item1)
        trainingData.append(item2)
        trainingData.append(item3)
    }
    
    func getForYouData(){
        let item1 = ForYouData(heading: "Joey Trribiani - The Machine",image:UIImage(named: "Image-2")!)
        let item2 = ForYouData(heading: "Chandler Bing - The Most Sarcastic Character Ever",image:UIImage(named: "Image-6")!)
        let item3 = ForYouData(heading: "Ross Geller - The Divorce Force",image:UIImage(named: "Image-10")!)
        forYouData.append(item1)
        forYouData.append(item2)
        forYouData.append(item3)
    }
    
    @IBAction func viewAllBtnPressed(_ sender: Any) {
        let controller =  storyboard?.instantiateViewController(identifier: "PlayVideoController") as! PlayVideoController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension VideosController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.newVideosCollectionViewOut{
            
            return newVideosData.count
            
        } else if collectionView == self.trainingCollectionViewOut {
            
            return trainingData.count
            
        } else {
            
            return data.count
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.newVideosCollectionViewOut{
     
            let newVideosCell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewVideosCollectionViewCell", for: indexPath) as! NewVideosCollectionViewCell
            newVideosCell.thumbnailOut.image = newVideosData[indexPath.row].image
//
            return newVideosCell
            
        } else if collectionView == self.trainingCollectionViewOut{
            
            let trainingCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrainingCollectionViewCell", for: indexPath) as! TrainingCollectionViewCell
            trainingCell.imageOut.image = trainingData[indexPath.row].image
            trainingCell.titleOut.text = trainingData[indexPath.row].title
            trainingCell.subtitleOut.text = trainingData[indexPath.row].subtitle
            
            return trainingCell
            
        } else {
            
            let forYouCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ForYouCollectionViewCell", for: indexPath) as! ForYouCollectionViewCell
            forYouCell.imageOut.image = forYouData[indexPath.row].image
//            forYouCell.headingOut.text = data[indexPath.row].heading
            
            return forYouCell
            
        }
    }
}


//MARK:- SERVICE TRAINING VIDEOS
extension VideosController{
    
//    VIDEO LIST
    private func getVideoList(){
    
        APIManager.sharedInstance.usersAPIManager.getVideoList{ (responseObject) in
            print("getVideoList response:-> ",responseObject)
//            let strainList = Mapper<StrainModel>().mapArray(JSONObject: responseObject) ?? [StrainModel]()
//            self.strainsList = strainList
//            print(self.strainsList)
//            self.tableViewOut.reloadData()
        } failure: { (error) in
            print(error)
        }
    }
    
//    TRAINING VIDEO LIST
    private func getTrainingVideoList(){
        let param : [String:Any] = ["video_category":227]
        APIManager.sharedInstance.usersAPIManager.getTrainingVideoList(param: param){ (responseObject) in
            print("getTrainingVideoList response:-> ",responseObject)
        } failure: { (error) in
            print(error)
        }
    }

//    NEW VIDEO LIST
    private func getNewVideoList(){
        let param : [String:Any] = ["video_category":228]
        APIManager.sharedInstance.usersAPIManager.getNewVideoList(param: param){ (responseObject) in
            print("getNewVideoList response:-> ",responseObject)
        } failure: { (error) in
            print(error)
        }
    }
   
    //    SINGLE VIDEO DETAIL
        private func getSingleVideoDetail(){
            let param : [String:Any] = ["id":"/40197"]
            APIManager.sharedInstance.usersAPIManager.getSingleVideoDetail(param: param){ (responseObject) in
                print("getSingleVideoDetail response:-> ",responseObject)
            } failure: { (error) in
                print(error)
            }
        }
    
}

//MARK:- SERVICE NEW VIDEOS
extension VideosController{
    
}
