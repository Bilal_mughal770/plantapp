//
//  MyPaymentMethodsController.swift
//  PlantApp
//
//  Created by Nayyer Ali on 11/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit

struct PaymentMethodsData {
    var cardName: String
    var cardNumber: String
}

class MyPaymentMethodsController: UIViewController {
    
    //MARK: Public Variables
    
    //MARK: Private Variables
    
    var paymentMethodsData = [PaymentMethodsData]()
    
    //MARK: Outlets
    
    @IBOutlet weak var tableViewOut: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewOut.delegate = self
        tableViewOut.dataSource = self
        getData()
    }
    
    func getData(){
        let item1 = PaymentMethodsData(cardName: "Visa Card", cardNumber: "4412-5232-4861-1511")
        let item2 = PaymentMethodsData(cardName: "Premium Card", cardNumber: "8154-6841-1351-1516")
        let item3 = PaymentMethodsData(cardName: "Debit Card", cardNumber: "1515-2123-1818-1215")
        let item4 = PaymentMethodsData(cardName: "Credit Card", cardNumber: "5498-5112-5151-8484")
        let item5 = PaymentMethodsData(cardName: "Union Card", cardNumber: "1151-8464-3121-8488")
        paymentMethodsData.append(item1)
        paymentMethodsData.append(item2)
        paymentMethodsData.append(item3)
        paymentMethodsData.append(item4)
        paymentMethodsData.append(item5)
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension MyPaymentMethodsController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentMethodsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyPaymentMethodsTableViewCell") as! MyPaymentMethodsTableViewCell
        cell.carrdNameOut.text = paymentMethodsData[indexPath.row].cardName
        cell.cardNumberOut.text = paymentMethodsData[indexPath.row].cardNumber
        return cell
    }
}
