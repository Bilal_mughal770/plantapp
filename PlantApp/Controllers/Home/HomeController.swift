//
//  HomeController.swift
//  PlantApp
//
//  Created by Nayyer Ali on 11/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit
import SwiftSpinner
import SDWebImage

class HomeController: UIViewController {
    
    //MARK: Public Variables
    
    //MARK: Private Variables
    
    private var nightTimeProducts = [ProductsByNightTime]()
    private var productsByCount = [ProductsByCount]()
    private var populerProducts = [ProductsByPopuler]()
    
    //MARK: Outlets
    
    @IBOutlet weak var nightCollectionViewOut: UICollectionView!
    @IBOutlet weak var typesCollectionView: UICollectionView!
    @IBOutlet weak var populerCollectionViewOut: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nightCollectionViewOut.delegate = self
        nightCollectionViewOut.dataSource = self
        typesCollectionView.delegate = self
        typesCollectionView.dataSource = self
        populerCollectionViewOut.delegate = self
        populerCollectionViewOut.dataSource = self
//        getProductsByNigtTimeUsage()
//        getProductsByCount()
        getPopularPlant()
        getPlantByTypes()
        getPlantByNight()
        
    }
    
    func getProductsByNigtTimeUsage(){
        ServerCommunication.shared.productsByNightTimeUsage { (status, message, data) in
            if status{
                self.nightTimeProducts = data!
                self.nightCollectionViewOut.reloadData()
                SwiftSpinner.hide()
            } else {
                SwiftSpinner.hide()
            }
        }
    }
    
    func getProductsByCount(){
        ServerCommunication.shared.productsByCount { (status, message, data) in
            if status{
                self.productsByCount = data!
                self.typesCollectionView.reloadData()
                SwiftSpinner.hide()
                self.getProductsPopulerThisWeek()
            } else {
                SwiftSpinner.hide()
            }
        }
    }
    
    func getProductsPopulerThisWeek(){
        ServerCommunication.shared.populerProducts{ (status, message, data) in
            if status{
                self.populerProducts = data!
                self.populerCollectionViewOut.reloadData()
                SwiftSpinner.hide()
            } else {
                SwiftSpinner.hide()
            }
        }
    }
}

extension HomeController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.nightCollectionViewOut{
            
            return nightTimeProducts.count
            
        } else if collectionView == self.typesCollectionView {
            
            return productsByCount.count
            
        } else {
            
            return populerProducts.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.nightCollectionViewOut{
            
            let nightCell = collectionView.dequeueReusableCell(withReuseIdentifier: "NightTimeCollectionViewCell", for: indexPath) as! NightTimeCollectionViewCell
            nightCell.titleOut.text = nightTimeProducts[indexPath.row].productName
            if let url = URL(string: nightTimeProducts[indexPath.row].productImageURL!) {
                nightCell.imageOut.sd_setImage(with: url, placeholderImage: UIImage(named: "Logo"), options: SDWebImageOptions.continueInBackground) { (image, error, cacheType, url) in
                }
            }
            return nightCell
            
        } else if collectionView == self.typesCollectionView{
            
            let typesCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TypesCollectionViewCell", for: indexPath) as! TypesCollectionViewCell
            typesCell.titleOut.text = productsByCount[indexPath.row].categoryName
            typesCell.quantityOut.text = "(\(productsByCount[indexPath.row].totalProduct))"
            if let url = URL(string: productsByCount[indexPath.row].categoryImageURL!) {
                typesCell.imageOut.sd_setImage(with: url, placeholderImage: UIImage(named: "Logo"), options: SDWebImageOptions.continueInBackground) { (image, error, cacheType, url) in
                }
            }
            return typesCell
            
        } else {
            
            let populerCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PopulerCollectionViewCell", for: indexPath) as! PopulerCollectionViewCell
            populerCell.titleOut.text = populerProducts[indexPath.row].productName
            if let url = URL(string: productsByCount[indexPath.row].categoryImageURL!) {
                populerCell.imageOut.sd_setImage(with: url, placeholderImage: UIImage(named: "Logo"), options: SDWebImageOptions.continueInBackground) { (image, error, cacheType, url) in
                }
            }
            return populerCell
        }
    }
}

//MARK:- SERVICE PLANT TYPES
extension HomeController{
//    POPULAR PLANT
    private func getPopularPlant(){
        
        APIManager.sharedInstance.usersAPIManager.getPopularPlant{ (responseObject) in
            print("getPopularPlant response:-> ",responseObject)
        } failure: { (error) in
            print(error)
        }
    }
//  PLANT BY TYPES
    private func getPlantByTypes(){
        
        APIManager.sharedInstance.usersAPIManager.getPlantByTypes{ (responseObject) in
            print("getPlantByTypes response:-> ",responseObject)
        } failure: { (error) in
            print(error)
        }
    }
    //    PLANT BY NIGHT TYPES
    private func getPlantByNight(){
        
        APIManager.sharedInstance.usersAPIManager.getPlantByNight{ (responseObject) in
            print("getPlantByNight response:-> ",responseObject)
        } failure: { (error) in
            print(error)
        }
    }
}


