//
//  TypesCollectionViewCell.swift
//  PlantApp
//
//  Created by Nayyer Ali on 23/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit

class TypesCollectionViewCell: UICollectionViewCell {
    
    //MARK: Outlets
    
    @IBOutlet weak var imageOut: CustomImage!
    @IBOutlet weak var titleOut: UILabel!
    @IBOutlet weak var quantityOut: UILabel!
}
