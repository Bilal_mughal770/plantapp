//
//  ForgotPasswordController.swift
//  PlantApp
//
//  Created by Nayyer Ali on 11/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit
import ObjectMapper

class ForgotPasswordController: UIViewController {
    
    //MARK: Public Variables
    
    //MARK: Private Variables
    
    //MARK: Outlets
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onBtnSubmit(_ sender: UIButton) {
        self.forgetPassword()
    }
}

//MARK:- Service
extension ForgotPasswordController{
    private func forgetPassword(){
        let param : [String:Any] = ["email":"/dtest@yopmail.com"]
        APIManager.sharedInstance.usersAPIManager.forgetPassword(param: param) { (responseObject) in
            print("forget password response:-> ",responseObject)
        } failure: { (error) in
            print(error.localizedDescription)
        }

    }
}
