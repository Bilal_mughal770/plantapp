//
//  NotificationsTableViewCell.swift
//  PlantApp
//
//  Created by Nayyer Ali on 23/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit

class NotificationsTableViewCell: UITableViewCell {
    
    //MARK: Outlets
    
    @IBOutlet weak var imageOut: CustomImage!
    @IBOutlet weak var notificationTitle: UILabel!
    @IBOutlet weak var notificationBody: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
