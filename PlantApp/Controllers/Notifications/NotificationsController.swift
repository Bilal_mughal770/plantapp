//
//  NotificationsController.swift
//  PlantApp
//
//  Created by Nayyer Ali on 11/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit

struct NotificationsData {
    var notificationTitle: String
    var notificationBody: String
}

class NotificationsController: UIViewController {
    
    //MARK: Public Variables
    
    //MARK: Private Variables
    
    var notificationsData = [NotificationsData]()
    
    //MARK: Outlets
    
    @IBOutlet weak var tableViewOut: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewOut.delegate = self
        tableViewOut.dataSource = self
        getData()
    }
    
    func getData(){
        let item1 = NotificationsData(notificationTitle: "Plants Delivered", notificationBody: "The required items have been delivered to the provided address")
        let item2 = NotificationsData(notificationTitle: "Cart Filled", notificationBody: "Your cart is filled with all required items")
        let item3 = NotificationsData(notificationTitle: "Water Plants", notificationBody: "You better water your plants every morning")
        notificationsData.append(item1)
        notificationsData.append(item2)
        notificationsData.append(item3)
       }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension NotificationsController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationsTableViewCell") as! NotificationsTableViewCell
        cell.notificationTitle.text = notificationsData[indexPath.row].notificationTitle
        cell.notificationBody.text = notificationsData[indexPath.row].notificationBody
        return cell
    }
}
