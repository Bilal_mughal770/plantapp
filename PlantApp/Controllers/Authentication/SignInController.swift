//
//  SignInController.swift
//  PlantApp
//
//  Created by Nayyer Ali on 11/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit
import SwiftSpinner
import ObjectMapper

class SignInController: UIViewController {
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailField.text = "devtest@gmail.com"
        passwordField.text = "dtest@1234"
    }
    
    @IBAction func unwind( _ seg: UIStoryboardSegue) {}
    
    @IBAction func signInBtnPressed(_ sender: Any) {
        self.login()
        
//        if emailField.text == "" || passwordField.text == ""{
//
//            print("Both fields are required to continue")
//
//        } else {
//
//            SwiftSpinner.show("Please Wait!")
//
//            let body = ["username": emailField.text, "password": passwordField.text]
//
//            ServerCommunication.shared.loginRequest(loginDetails: body as [String : Any]) { (status, message) in
//                if status {
//                    SwiftSpinner.hide()
//                    self.navigateToHome()
//                } else {
//                    SwiftSpinner.hide()
//               print("Error here...")
//                }
//            }
//        }
    }
    
    @IBAction func forgotPasswordBtnPressed(_ sender: Any) {
        navigateToForgotPassword()
    }
    
    @IBAction func signUpBtnPressed(_ sender: Any) {
        navigateToSignUp()
    }
    
    @IBAction func facebookBtnPressed(_ sender: Any) {
    }
    
    @IBAction func googleBtnPressed(_ sender: Any) {
    }
    
    func navigateToHome(){
        //    let controller =  storyboard?.instantiateViewController(identifier: "DiscoverController") as! DiscoverController
            let controller =  storyboard?.instantiateViewController(identifier: "HomeController") as! HomeController
            self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func navigateToForgotPassword(){
        let controller =  storyboard?.instantiateViewController(identifier: "ForgotPasswordController") as! ForgotPasswordController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func navigateToSignUp(){
        let controller =  storyboard?.instantiateViewController(identifier: "SignUpController") as! SignUpController
        self.navigationController?.pushViewController(controller, animated: true)
    }
}


extension SignInController {
    
    private func login(){
        let param : [String:Any] = ["username": emailField.text, "password": passwordField.text]

        APIManager.sharedInstance.usersAPIManager.LoginUser(params: param) { (responseObject) in
            let response = responseObject as Dictionary
            let user = Mapper<UserModel>().map(JSON: response) ?? UserModel()
            AppStateManager.sharedInstance.loginUser(user: user)
            self.navigateToHome()
            print(responseObject)
        } failure: { (error) in
            print("my errrrrrr",error.localizedDescription)
        }

    }
}
