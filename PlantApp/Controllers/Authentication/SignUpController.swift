//
//  SignUpController.swift
//  PlantApp
//
//  Created by Nayyer Ali on 11/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit

class SignUpController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func signInBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signUpBtnPressed(_ sender: Any) {
//               let controller =  storyboard?.instantiateViewController(identifier: "DiscoverController") as! DiscoverController
            let controller =  storyboard?.instantiateViewController(identifier: "HomeController") as! HomeController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}
