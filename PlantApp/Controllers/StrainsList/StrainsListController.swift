//
//  StrainsListController.swift
//  PlantApp
//
//  Created by Nayyer Ali on 11/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit

import ObjectMapper
class StrainsListController: UIViewController {
    
    //MARK: Public Variables
    
    //MARK: Private Variables
    var strainsList = [StrainModel]()
    
    //MARK: Outlets
    
    @IBOutlet weak var tableViewOut: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewOut.delegate = self
        self.tableViewOut.dataSource = self
        self.strainList()
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension StrainsListController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.strainsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: StrainsTableViewCell.identifier ) as! StrainsTableViewCell
        let data = self.strainsList[indexPath.row]
        cell.setData(data: data)
        return cell
    }
}

//MARK:- SERVICE
extension StrainsListController{
    private func strainList(){
        APIManager.sharedInstance.usersAPIManager.getStrainList { (responseObject) in
            print("strainList response:-> ",responseObject)
            let strainList = Mapper<StrainModel>().mapArray(JSONObject: responseObject) ?? [StrainModel]()
            self.strainsList = strainList
            print(self.strainsList)
            self.tableViewOut.reloadData()
        } failure: { (error) in
            print(error)
        }
    }
}
