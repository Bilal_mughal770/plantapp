//
//  StrainsTableViewCell.swift
//  PlantApp
//
//  Created by Nayyer Ali on 23/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit
import SDWebImage
class StrainsTableViewCell: UITableViewCell {
    
    //MARK: Outlets
    
    @IBOutlet weak var subtitleOut: UILabel?
    @IBOutlet weak var titleOut: UILabel?
    @IBOutlet weak var imageOut: CustomImage?
    
     func setData(data:StrainModel){
        self.titleOut?.text = data.productName ?? ""
        self.subtitleOut?.text = "\(data.productId)"
        if let url = URL(string: data.productImageUrl ?? "" ) {
            self.imageOut?.sd_setImage(with: url, placeholderImage: UIImage(named: "Logo"), options: SDWebImageOptions.continueInBackground) { (image, error, cacheType, url) in
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
