//
//  MyAddresessController.swift
//  PlantApp
//
//  Created by Nayyer Ali on 11/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit

struct AddressData {
    var addressType: String
    var address: String
}

class MyAddresessController: UIViewController {
    
    //MARK: Public Variables
    
    //MARK: Private Variables

    var addressData = [AddressData]()
    
    //MARK: Outlets
    
    @IBOutlet weak var tableViewOut: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewOut.delegate = self
        tableViewOut.dataSource = self
        getData()
    }
    
    func getData(){
        let item1 = AddressData(addressType: "Home", address: "House no L-531 area Korangi 2-1/2, Karachi")
        let item2 = AddressData(addressType: "Office", address: "Suit no 2 area Gulshan Iqbal, Karachi")
        let item3 = AddressData(addressType: "Friends Place", address: "Street 2 Manchester Road")
        let item4 = AddressData(addressType: "College", address: "New York, Times Square")
        addressData.append(item1)
        addressData.append(item2)
        addressData.append(item3)
        addressData.append(item4)
    }
    
    @IBAction func deleteBtnPressed(_ sender: Any) {
        
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension MyAddresessController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addressData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyAddressesTableViewCell") as! MyAddressesTableViewCell
        cell.addressTypeOut.text = addressData[indexPath.row].addressType
        cell.addressOut.text = addressData[indexPath.row].address
        return cell
    }
}
