//
//  DiagnosisCompletedController.swift
//  PlantApp
//
//  Created by Nayyer Ali on 11/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit

class DiagnosisCompletedController: UIViewController {
    
    //MARK: Public Variables
    
    //MARK: Private Variables
    
    //MARK: Outlets
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func homeBtnPressed(_ sender: Any) {
                let controller =  storyboard?.instantiateViewController(identifier: "HomeController") as! HomeController
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
