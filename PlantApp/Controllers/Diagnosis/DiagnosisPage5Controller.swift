//
//  DiagnosisPage5Controller.swift
//  PlantApp
//
//  Created by Nayyer Ali on 11/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit

class DiagnosisPage5Controller: UIViewController {
    
    //MARK: Public Variables
    
    //MARK: Private Variables
    
    //MARK: Outlets
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonPressed(_ sender: Any) {
        let controller =  storyboard?.instantiateViewController(identifier: "DiagnosisPage6Controller") as! DiagnosisPage6Controller
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}
