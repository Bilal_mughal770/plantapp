//
//  DiagnosisPage1Controller.swift
//  PlantApp
//
//  Created by Nayyer Ali on 11/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit

class DiagnosisPage1Controller: UIViewController {

    //MARK: Public Variables
    
    //MARK: Private Variables
    
    //MARK: Outlets
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func outsideBtnPressed(_ sender: Any) {
        let controller =  storyboard?.instantiateViewController(identifier: "DiagnosisPage2Controller") as! DiagnosisPage2Controller
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func insideBtnPressed(_ sender: Any) {
        let controller =  storyboard?.instantiateViewController(identifier: "DiagnosisPage2Controller") as! DiagnosisPage2Controller
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}
