//
//  SettingsController.swift
//  PlantApp
//
//  Created by Nayyer Ali on 11/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit

class SettingsController: UIViewController {
    
    //MARK: Public Variables
    
    //MARK: Private Variables
    
    //MARK: Outlets
    
    @IBOutlet weak var notificationsBtn: UISwitch!
    @IBOutlet weak var themeBtn: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.bool(forKey: isDarkModeOn){
            themeBtn.isOn = true
        }
    }
    
    @IBAction func toggleNotifications(_ sender: Any) {
        
        switch notificationsBtn.isOn {
            
        case true:
            print("You will receive all imporatnt notifications")
        case false:
            print("You will not able to receive any notificationYou will receive all imporatnt notifications")
        }
    }
    
    @IBAction func toggleTheme(_ sender: Any) {
        
        switch themeBtn.isOn {
            
        case true:
            NotificationCenter.default.post(name: darkThemeOn, object: nil)
            UserDefaults.standard.set(true, forKey: isDarkModeOn)
            
        case false:
            NotificationCenter.default.post(name: darkThemeOff, object: nil)
            UserDefaults.standard.set(false, forKey: isDarkModeOn)
        }
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func myPaymentMethodsBtnPressed(_ sender: Any) {
        let controller =  storyboard?.instantiateViewController(identifier: "MyPaymentMethodsController") as! MyPaymentMethodsController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func myAddressedBtnPressed(_ sender: Any) {
        let controller =  storyboard?.instantiateViewController(identifier: "MyAddresessController") as! MyAddresessController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func changePasswordBtnPressed(_ sender: Any) {
        let controller =  storyboard?.instantiateViewController(identifier: "ChangePasswordController") as! ChangePasswordController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func languageBtnPressed(_ sender: Any) {
        let controller =  storyboard?.instantiateViewController(identifier: "LanguageController") as! LanguageController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func helpBtnPressed(_ sender: Any) {
        let controller =  storyboard?.instantiateViewController(identifier: "HelpCenterController") as! HelpCenterController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func privacyPolicyBtnPressed(_ sender: Any) {
        let controller =  storyboard?.instantiateViewController(identifier: "PrivacyPolicyController") as! PrivacyPolicyController
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
