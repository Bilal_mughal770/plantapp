//
//  AddAddressController.swift
//  PlantApp
//
//  Created by Nayyer Ali on 11/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit
import TextFieldEffects

class AddAddressController: UIViewController {
    
    //MARK: Public Variables
    
    //MARK: Private Variables
    
    //MARK: Outlets
    
    @IBOutlet weak var defaultAddressBtnOut: UIButton!
    @IBOutlet weak var addressField: HoshiTextField!
    @IBOutlet weak var buildingField: HoshiTextField!
    @IBOutlet weak var noteField: HoshiTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func makeThisDefaultAddressBtnPressed(_ sender: Any) {
    }
    
    @IBAction func saveAddressBtnPressed(_ sender: Any) {
    }
}
