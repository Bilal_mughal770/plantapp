//
//  PlayVideoController.swift
//  PlantApp
//
//  Created by Nayyer Ali on 11/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit

struct CommentsData {
    var name: String
    var comment: String
    var image: UIImage
}

struct RelatedVideosData {
    var title: String
    var views: String
    var thumbnail: UIImage
}

class PlayVideoController: UIViewController {

    //MARK: Public Variables
    
    //MARK: Private Variables
    
    var commentsData = [CommentsData]()
    var relatedVideosData = [RelatedVideosData]()
    
    //MARK: Outlets
    
    @IBOutlet weak var collectionViewOut: UICollectionView!
    @IBOutlet weak var tableViewOut: UITableView!
    @IBOutlet weak var totalCommentsOut: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewOut.delegate = self
        tableViewOut.dataSource = self
        collectionViewOut.delegate = self
        collectionViewOut.dataSource = self
        getCommentsData()
        getRelatedVideosData()
    }
    
    func getCommentsData(){
        let item1 = CommentsData(name: "Nayyer Ali", comment: "This is the best thing on the planet", image: UIImage(named: "Image-7")!)
        let item2 = CommentsData(name: "Hamza Khan", comment: "Best ever experience", image: UIImage(named: "8")!)
        let item3 = CommentsData(name: "Tom Cruise", comment: "Developement has never been easy", image: UIImage(named: "Image-9")!)
        let item4 = CommentsData(name: "Johny Depp", comment: "This is my 2 lines comment to verify view on big scale", image: UIImage(named: "Image-10")!)
        commentsData.append(item1)
        commentsData.append(item2)
        commentsData.append(item3)
        commentsData.append(item4)
    }
    
    func getRelatedVideosData(){
        let item1 = RelatedVideosData(title: "Water Plantation", views: "99K", thumbnail: UIImage(named: "25")!)
        let item2 = RelatedVideosData(title: "Memories", views: "10K", thumbnail: UIImage(named: "24")!)
        let item3 = RelatedVideosData(title: "See You Again!", views: "1M", thumbnail: UIImage(named: "23")!)
        relatedVideosData.append(item1)
        relatedVideosData.append(item2)
        relatedVideosData.append(item3)
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension PlayVideoController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        totalCommentsOut.text = "\(commentsData.count) Comments"
        return commentsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentsTableViewCell") as! CommentsTableViewCell
        cell.nameOut.text = commentsData[indexPath.row].name
        cell.commentOut.text = commentsData[indexPath.row].comment
        cell.imageOut.image = commentsData[indexPath.row].image
        return cell
    }
}

extension PlayVideoController:UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return relatedVideosData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RelatedVideosCollectionViewCell", for: indexPath) as! RelatedVideosCollectionViewCell
        cell.thumbnailOut.image = relatedVideosData[indexPath.row].thumbnail
        cell.titleOut.text = relatedVideosData[indexPath.row].title
        cell.viewsOut.text = relatedVideosData[indexPath.row].views
        
        return cell
    }
}
