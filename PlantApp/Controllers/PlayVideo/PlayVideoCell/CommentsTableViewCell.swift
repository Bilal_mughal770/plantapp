//
//  CommentsTableViewCell.swift
//  PlantApp
//
//  Created by Nayyer Ali on 25/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit

class CommentsTableViewCell: UITableViewCell {
    
    //MARK: Outlets
    
    @IBOutlet weak var imageOut: CustomImage!
    @IBOutlet weak var nameOut: UILabel!
    @IBOutlet weak var commentOut: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
