//
//  RelatedVideosCollectionViewCell.swift
//  PlantApp
//
//  Created by Nayyer Ali on 25/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit

class RelatedVideosCollectionViewCell: UICollectionViewCell {
    
    //MARK: Outlets
    
    @IBOutlet weak var thumbnailOut: CustomImage!
    @IBOutlet weak var titleOut: UILabel!
    @IBOutlet weak var viewsOut: UILabel!
}
