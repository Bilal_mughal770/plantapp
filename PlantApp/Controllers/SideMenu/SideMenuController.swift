//
//  SideMenuController.swift
//  PlantApp
//
//  Created by Nayyer Ali on 16/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit

class SideMenuController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func requestFormBtnPressed(_ sender: Any) {
        let controller =  storyboard?.instantiateViewController(identifier: "StrainRequestFormController") as! StrainRequestFormController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func logOutBtnPressed(_ sender: Any) {
        AppStateManager.sharedInstance.processLogoutUser()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let controller =  storyboard?.instantiateViewController(identifier: "EditProfileController") as! EditProfileController
            self.navigationController?.pushViewController(controller, animated: true)
        case 1:
            break;
        case 2:
//            let controller =  storyboard?.instantiateViewController(identifier: "HomeController") as! HomeController
//            self.navigationController?.pushViewController(controller, animated: true)
            let controller =  storyboard?.instantiateViewController(identifier: "DiscoverController") as! DiscoverController
            self.navigationController?.pushViewController(controller, animated: true)
        case 3:
            let controller =  storyboard?.instantiateViewController(identifier: "StrainsListController") as! StrainsListController
            self.navigationController?.pushViewController(controller, animated: true)
        case 4:
            let controller =  storyboard?.instantiateViewController(identifier: "ProductDetailController") as! ProductDetailController
            self.navigationController?.pushViewController(controller, animated: true)
        case 5:
//            let controller =  storyboard?.instantiateViewController(identifier: "RateExperienceController") as! RateExperienceController
//            self.navigationController?.pushViewController(controller, animated: true)
            break;
        case 6:
            let controller =  storyboard?.instantiateViewController(identifier: "VideosController") as! VideosController
            self.navigationController?.pushViewController(controller, animated: true)
        case 7:
            let controller =  storyboard?.instantiateViewController(identifier: "ArticlesReadController") as! ArticlesReadController
            self.navigationController?.pushViewController(controller, animated: true)
            break;
        case 8:
            let controller =  storyboard?.instantiateViewController(identifier: "DiagnosisPage1Controller") as! DiagnosisPage1Controller
            self.navigationController?.pushViewController(controller, animated: true)
        case 9:
            break;
        case 10:
//            let controller =  storyboard?.instantiateViewController(identifier: "MyCartController") as! MyCartController
//            self.navigationController?.pushViewController(controller, animated: true)
            break;
        case 11:
            let controller =  storyboard?.instantiateViewController(identifier: "MyWishlistController") as! MyWishlistController
            self.navigationController?.pushViewController(controller, animated: true)
        case 12:
            let controller =  storyboard?.instantiateViewController(identifier: "SettingsController") as! SettingsController
            self.navigationController?.pushViewController(controller, animated: true)
        case 13:
            break;
        case 14:
            let controller =  storyboard?.instantiateViewController(identifier: "StrainRequestFormController") as! StrainRequestFormController
            self.navigationController?.pushViewController(controller, animated: true)
        case 15:
            AppStateManager.sharedInstance.processLogoutUser()
//            self.navigationController?.popToRootViewController(animated: true)
        default:
            break;
        }
    }
}


//MARK:- NAVIGATION FUNCTION

extension SideMenuController{
   private func pushTo(controllerIdentifier: String) {
        guard let topController = Utility.main.topViewController() else {return}
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: controllerIdentifier)
        topController.navigationController?.pushViewController(controller, animated: true)
    }
}
