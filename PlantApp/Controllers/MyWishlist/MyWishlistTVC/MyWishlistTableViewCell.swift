//
//  MyWishlistTableViewCell.swift
//  PlantApp
//
//  Created by Nayyer Ali on 23/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit

class MyWishlistTableViewCell: UITableViewCell {
    
    //MARK: Outlets
    
    @IBOutlet weak var priceOut: UILabel!
    @IBOutlet weak var nameOut: UILabel!
    @IBOutlet weak var imageOut: CustomImage!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
