//
//  MyWishlistController.swift
//  PlantApp
//
//  Created by Nayyer Ali on 11/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit

struct MyWishlistData {
    var name: String
    var price: String
    var image: UIImage
}

class MyWishlistController: UIViewController {
    
    //MARK: Public Variables
    
    //MARK: Private Variables
    
    var myWishlistData = [MyWishlistData]()
    
    //MARK: Outlets
    
    @IBOutlet weak var tableViewOut: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewOut.delegate = self
        tableViewOut.dataSource = self
        getData()
    }
    
    func getData(){
        let item1 = MyWishlistData(name: "Nayyer Ali", price: "$15", image: UIImage(named: "7")!)
        let item2 = MyWishlistData(name: "Hamza Khan", price: "$25", image: UIImage(named: "8")!)
        let item3 = MyWishlistData(name: "Tom Cruise", price: "$35", image: UIImage(named: "9")!)
        let item4 = MyWishlistData(name: "Johny Depp", price: "$45", image: UIImage(named: "10")!)
        let item5 = MyWishlistData(name: "Robert Downey Jr.", price: "$55", image: UIImage(named: "11")!)
        let item6 = MyWishlistData(name: "Chris Hemsworth", price: "$65", image: UIImage(named: "12")!)
        let item7 = MyWishlistData(name: "Chris Evans", price: "$75", image: UIImage(named: "13")!)
        let item8 = MyWishlistData(name: "Chris Pratt", price: "$85", image: UIImage(named: "14")!)
        let item9 = MyWishlistData(name: "Dave Batista", price: "$95", image: UIImage(named: "15")!)
        myWishlistData.append(item1)
        myWishlistData.append(item2)
        myWishlistData.append(item3)
        myWishlistData.append(item4)
        myWishlistData.append(item5)
        myWishlistData.append(item6)
        myWishlistData.append(item7)
        myWishlistData.append(item8)
        myWishlistData.append(item9)
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension MyWishlistController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myWishlistData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyWishlistTableViewCell") as! MyWishlistTableViewCell
        cell.nameOut.text = myWishlistData[indexPath.row].name
        cell.priceOut.text = myWishlistData[indexPath.row].price
        cell.imageOut.image = myWishlistData[indexPath.row].image
        return cell
    }
}
