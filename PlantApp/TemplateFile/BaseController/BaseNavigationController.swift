//
//  BaseNavigationController.swift
//  PlantApp
//
//  Created by Muhammad Osama Afaque on 25/07/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import Foundation
import UIKit

class BaseNavigationController: UINavigationController {
    
    let navBarColor = UIColor(red: 138, green: 200, blue: 49, alpha: 1)
    
    //    MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.barTintColor = UIColor(red: 138.0/255.0, green: 200.0/255.0, blue: 49.0/255.0, alpha: 1.0)
        self.navigationBar.tintColor = .white
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        self.navigationBar.isTranslucent = false
    }
}
