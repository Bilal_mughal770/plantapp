//
//  Strings.swift
//  S2OCustomer
//
//  Created by Fazeel Ahmed on 15/04/2021.
//  Copyright © 2021 Mustafa Siddique. All rights reserved.
//

import Foundation

enum Strings: String{
    
    //SingleMethod For the Usage of Localization.
    var text: String { return NSLocalizedString( self.rawValue, comment: "") }
    
    //MARK:- Notifies
    case ALERT = "Alert"
    case ERROR = "Error"
    case UNKNOWN_ERROR = "Unknown error"
    case YES = "Yes"
    case NO = "No"
    case OK = "Ok"
    case CANCEL = "Cancel"
    case CONTINUE = "Continue"
    case CONFIRM = "Confirm"
    case LATER = "Later"
    case SUCCESS = "Success"
    case CONFIRMATION = "Confirmation"
    case PIN_SENT_EMAIL = "A verification PIN has been sent to your provided email address."
    
    //MARK:- Validation
    case EMPTY_LOGIN_FIELDS = "Please provide your valid email address or mobile number to login."
    case PROFILE_IMAGE_REQUIRED = "Please upload your profile picture."
    case INVALID_NAME = "Name should contain atleast 3 characters."
    case INVALID_NAME_SPECIAL_CHAR_NUMBER = "Please provide valid name without Numbers and Special Characters"
    case INVALID_F_NAME = "Please provide a valid first name."
    case INVALID_L_NAME = "Please provide a valid last name."
    case INVALID_EMAIL = "Please provide a valid Email Address."
    case EMPTY_EMAIL = "Please enter email address"
    case INVALID_PHONE = "Please provide a valid Phone Number."
    case INVALID_OTP = "Please provide a valid OTP Code."
    case INVALID_COUNTRY = "Please select your country."
    case INVALID_CITY = "Please enter your city."
    case INVALID_ZIP_CODE = "Please enter valid zip code."
    case EMPTY_PWD = "Please enter password."
    case OLD_PWD_AND_NEW_PWD = "Old password and New password can not be same."
    case EMPTY_OLD_PWD = "Please enter old password."
    case EMPTY_NEW_PWD = "Please enter new password."
    case EMPTY_CONFIRM_PWD = "Please enter confirm password."
    case INVALID_PWD_LENGTH = "Password should contain minimum of 8 characters."
    case INVALID_PWD = "Password should contain minimum of 8 characters with atleast 1 uppercase, 1 lowercase and 1 digit."
    case PWD_ATLEAST_SIX_CH = "Password should contain at least 6 characters."
    case PWD_DONT_MATCH = "Password and confirm password does not match."
    case ALL_FIELD_REQ = "All Fields are required!"
    case INVALID_PIN = "Please provide complete PIN."
    case PWD_CHANGED = "You have successfully updated your password."
    case URL_NOT_VALID = "Invalid URL"
    case LOGOUT = "Logout"
    case ASK_LOGOUT = "Are you sure you want to logout?"
    case EMPTY_ADDRESS = "Please enter your address."
    case ASK_CONTINUE_WITH_PREVIOUS_CARD = "Continue paying with card \n **** **** **** "
    
    case RESPONSE_ERROR = "Invalid response for route:"
    case ERROR_GENERIC_MESSAGE = "Unable to connect server\n Please check your internet connection and try again later."
    case TOKEN_EXPIRED = "Invalid Authentication Token Supplied."
    case PASSWORD_UPDATED = "Password updated successfully."
    case MESSAGE_DELIVERED = "Message delivered"
    case CONTACT_US_SUBMITTED = "Our team will respond you shortly."
    case PROFILE_UPDATED = "Profile updated successfully."
    
    //MARK:- Confirmations
    case SIGNUP = "Sign-up"
    case SIGNUP_SUCCESSFUL = "Sign-up successfully\nPlease login to continue."
    
    case TERMS_AND_POLICIES = "By using Utorny you agree to the Terms of Services and Privacy Policy"
    case TERMS_AND_CONDITIONS_OF_UTORNY = "You agree to the Terms and Conditions of Utorny App"
    case TERMS_AND_CONDITIONS_ON_NEW_CASE = "By pressing Submit, you agree to the Terms and Conditions of Utorny."
    
    //MARK:- Applications specific strings
    case LOGIN_AS_USER = "Please enter credential to login as user"
    case LOGIN_AS_LAWYER = "Please enter credential to login as lawyer"
    case SKIP = "Skip"
    
    case NOTIFICATION = "Notification"
    case LANGUAGE = "Language"
    case EDIT_PROFILE = "Edit Profile"
    case LOCAL_AUTHENTICATION = "Local Authentication"
    case CHANGE_PASSWORD = "Change Password"
    case TERMS_AND_CONDITION = "Terms and Conditions"
    case ABOUT_US = "About us"
    case RATE_THE_APP = "Rate the app"
    case CONTACT_US = "Contact Us"
    case SIGN_OUT = "Sign Out"
    case SIGN_IN = "Sign In"
    case ABOUT = "About"
    
    case OLD = "Old"
    case NEW = "New"
    case ONGOING = "On Going"
    case PAST_CASES = "Past Cases"
    case MY_CASES = "My Cases"
    
    //MARK:- Controller titles
    case SETTINGS = "Settings"
    case TERMS_OF_SERVICES = "Terms of Services"
    case PRIVACY_POLICY = "Privacy Policy"
    case ADD_NEW_CASE = "Add a new case"
    case ALL_CATEGORIES = "All Categories"
    case SCHEDULE_DATE = "Schedule Date"
    case CASE_FILES = "Case Files"
    case MY_BIDS = "My Bids"
    case MY_APPEALS = "My Appeals"
    
    case GUEST = "Guest"
    case PLEASE_SELECT_CITY = "Please select your city."
    case PLEASE_SELECT_PLAN = "Please select payment plan."
    case INVALID_PERMIT_NUMBER = "Please enter your legal permit number."
    case CODE_RESENT = "Verification code has been sent to phone number."
    case VERIFY_ACCOUNT = "Please verify your account"
    
    case MALE = "Male"
    case FEMALE = "Female"
    
    case SELECT_FILTER_FIRST = "Please select filter."
    case SELECT_COUNTRY = "Please select country."
    case SELECT_CITY = "Please select city"
    case INVALID_CITY_INPUT = "Please provide valid city"
    case SELECT_CATEGORY = "Please select category"
    case ENTER_TITLE = "Please enter title"
    case SELECT_BUDGET = "Please type budget"
    case SELECT_AMOUNT_TYPE = "Please select amount type"
    case INVALID_BUDGET_AMOUNT = "Please enter valid budget amount"
    case INVALID_BUDGET_PERCENTAGE = "Please enter valid budget percentage"
    case SELECT_CASE_TYPE = "Please select case type"
    case ENTER_CASE_DETAIL = "Please enter case detail"
    
    case NEW_CASE_POSTED = "Case has been submitted for review"
    
    case NO_SCHEDULES = "No schedules available."
    case NO_SCHEDULES_DESC = "When you have schedules, you'll see them here."
    
    case NO_NEW_CASE = "No new case(s) available."
    case NO_NEW_CASE_DESC = "When you have new case(s), you'll see them here."
    
    case NO_ONGOING_CASES = "No ongoing case(s) available."
    case NO_ONGOING_CASES_DESC = "When you have ongoing case(s), you'll see them here."
    
    case NO_PAST_CASES = "No past case(s) available."
    case NO_PAST_CASES_DESC = "When you have past case(s), you'll see them here."
    
    case NO_BIDS = "No bid(s) available."
    case NO_BIDS_DESC = "When you have bid(s), you'll see them here."
    
    case NO_NOTIFICATIONS = "No notification(s) available."
    case NO_NOTIFICATIONS_DESC = "When you have notification(s), you'll see them here."
    
    case NO_CONVERSATIONS = "No conversation(s) available."
    case NO_CONVERSATIONS_DESC = "When you have conversation(s), you'll see them here."
    
    case ASK_TO_ACCEPT_BID = "Are you sure, you want to accept this proposal?"
    case ASK_TO_REJECT_BID = "Are you sure, you want to decline this proposal?"
    case ASK_TO_TO_SUGGEST_QUOTE = "Are you sure, you want to suggest new quote>"
    case INVALID_SUGGESTED_QUOTE = "Invalid suggested quote"
    case INVALID_FINAL_QUOTE = "Invalid final quote"
    case PROPOSAL_ACCEPTED = "PROPOSAL ACCEPTED"
    case PROPOSAL_ACCEPTED_ON = "Proposal accepted by client on"
    case CASE_COMPLETED_ON = "Case completed on"
    case RECENT_TITLE = "Recent"
    case CASE_TITLE = "Case"
    case STATUS_TITLE = "Status"
    case MARK_COMPLETED = "MARK COMPLETED"
    case RATE_LAWYER = "Please rate your lawyer's service"
    case RATE_CLIENT = "Please rate your client's service"
    case MARK_COMPLETE_BY_LAWYER = "Your case has been mark completed. Please take out this and review your client now."
    
    case PHOTO_ALREADY_ADDED = "Photo already added."
    case VIDEO_ALREADY_ADDED = "Video already added."
    case DOCUMENT_ALREADY_ADDED = "Document already added."
    
    case PHOTO_ADDED_SUCCESSFULLY = "Photo added successfully."
    case VIDEO_ADDED_SUCCESSFULLY = "Video added successfully."
    case DOCUMENT_ADDED_SUCCESSFULLY = "Document added successfully."
    case DOCUMENT_FORMAT_NOT_SUPPORTED = "Document format not supported!"
    
    case UPLOAD_DOCMENT_IMAGE = "Upload document image"
    case ASK_UPLOAD_DOCMENT_IMAGE_VIA = "How do you want to set your document?"
    case ASK_UPLOAD_DOCMENT_PHOTO_VIA = "How do you want to set your photo?"
    case GALLERY = "Gallery"
    case CAMERA = "Camera"
    case VIDEO = "Video"
    case ADD_MESSAGE_FOR_THE_CLIENT = "Add a message for the client"
    case BID_SUBMITTED = "You have successfully bid this case."
    
    case ENTER_BIO = "Please enter your bio"
    case SELECT_LANGUAGE = "Please select you language(s)"
    case SELECT_CATEGORIES = "Please select you category/categories"
    case ENTER_SUCCESSFUL_CASES_COUNT = "Please enter your successfull cases in career"
    case SELECT_GENDER = "Please select your gender"
    case SELECT_DATE_OF_BIRTH = "Please select your date of birth"
    case ENTER_LEGAL_PERMIT_NUMBER = "Please enter your valid permit number"
    case IDENTIFY_YOURSELF = "Please verify your identity"
    
    case APPLICATION_WILL_RESTART = "The application will restart to change the language"
    
    case AED = "AED"
    case MARK_CASE_URGENT = "Yes, mark my case urgent ("
    case MARK_CASE_FEATURED = "Yes, mark my case featured ("
    case PER_DAY = "Per Day)"
    
    case TYPE_MESSAGE = "Please type your message..."
    case TYPE_FEEDBACK = "Please type your feedback..."
    
    case NO_LOGGED_IN_USER = "Please login from your email and password"
    case CASE_ID = "Case ID:"
    case ON = "on"
    case YOU_BID = "You bid"
    case APPROVED_BUDGET = "Approved Budget"
    case NO_BID_RECEIVED = "No bid received"
    case BID_RECEIVED = "Bid received"
    
    case USE_FACE_ID_TO_LOGIN = "Or use face id to login"
    case USE_TOUCH_ID_TO_LOGIN = "Or use touch id to login"
    
    case USE_FACE_ID_TO_CONTINUE = "Use face id to contine"
    case USE_TOUCH_ID_TO_CONTINUE = "Use touch id to contine"
    
    case WEEK = "Week";
    
    case JANUARY = "January"
    case FEBRUARY = "February"
    case MARCH = "March"
    case APRIL = "April"
    case MAY = "May"
    case JUNE = "June"
    case JULY = "July"
    case AUGUST = "August"
    case SEPTEMBER = "September"
    case OCTOBER = "October"
    case NOVEMBER = "November"
    case DECEMBER = "December"
    
    case TOKENS_AVAILABLE = "Tokens available"
    case TOKENS = "Tokens"
    case ARE_YOU_SURE_TO_BUY_THIS = "Are you sure to buy this?"
    case ARE_YOU_SURE_COMPLETE_THIS_CASE = "Are you sure you want to mark this case completed?"
    case ARE_YOU_SURE_DELETE_THIS_CASE = "Are you sure you want to delete this case?"
    case ARE_YOU_SURE_DELETE_ALL_NOTIFICATIONS = "Are you sure you want to delete all notifications?"
    case ARE_YOU_SURE_DELETE_THIS_NOTIFICATION = "Are you sure you want to delete this notification?"
    case PLAN_PURCHASED = "You have successfully purchased the plan."
    case OPEN_CASES_AT_THE_MOMENT = "open case(s) at the moment"
    case PENDING_ADMIN_APPROVAL = "Pending admin approval"
    case NO_MESSAGES_YET = "No message(s) yet"
    case DECLINED = "DECLINED"
    case RECEIVED_NEW_BID_FROM = "You received a new bid from"
    case NEW_QUOTE = "(New Quote)"
    
    case CASE_TITLE_ = "Case Title:"
    case CASE_OWNER = "Case Owner:"
    case REVIEWS = "Reviews"
    case FINAL_QUOTE_SUBMITTED = "Your final quote has been submitted successfully"
    case SEND_MESSAGE_TO = "SEND MESSAGE TO"
    case NO_FINAL_AMOUNT_PROPOSED = "No final amount proposed by lawyer yet"
    
    case PAYMENTS = "Payments"
    case ALL_CASES = "All Cases"
    case CLIENT_BUDGET = "Budget"
    
    case LAWYER_COUNTRY_ID_MISMATCHES = "This case doesn't matches your country of practice"
    
    case POST_CASE = "Post Case"
    case POST_CASE_REVIEW = "Please review the case details before you submit as they can't be edited later."
    case POST_CASE_CONTINUE = "Continue to Submit?"
    case CONTACT_MESSAGE_SENT = "You message has been sent successfully"
    case OPEN = "Open"
    case VERSION = "Version: "
    case WRITE_FEEDBACK = "Write your feedback here..."
    case VIEW = "View"
    case CASE_BIDS = "Case Bids"
    case REPOST = "Repost"
    case RATE_APP = "Will be activated once app is approved on App Store"
    case ENTER_CASE_DETAILS = "Enter Case Details"
    case RESET_CODE = "Resend Code"
    case DECLINED_CASE = "This case is declined by the client"
    case MAX_LIMIT = "Maximum size of 25MB limit reached, you can not add more files"
    case COMPLETE_YOUR_PROFILE = "Complete Your Profile"
    case COMPLETE_YOUR_PROFILE_DESC = "Please complete your profile before you start the bidding process"
}
