//
//  APIManagerBase.swift
//  S2OCustomer
//
//  Created by Fazeel Ahmed on 15/04/2021.
//  Copyright © 2021 Mustafa Siddique. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON


class APIManagerBase: NSObject {
    let baseURL = Constants.BaseURL
    let defaultRequestHeader = ["Content-Type": "application/json"]
    let defaultError = NSError(domain: "Error", code: 0, userInfo: [NSLocalizedDescriptionKey: "Request Failed."])
    
    func getAuthorizationHeader () -> Dictionary<String,String> {
        if let token = APIManager.sharedInstance.serverToken {
            return ["Authorization":"Bearer \(token)","Content-Type": "application/json-patch+json"]
        }
        return ["Content-Type":"application/x-www-form-urlencoded"]
    }
    
    func URLforRoute(route: String,params:[String: Any]) -> NSURL? {
        if let components: NSURLComponents  = NSURLComponents(string: (Constants.BaseURL+route)){
            var queryItems = [NSURLQueryItem]()
            for(key,value) in params {
                queryItems.append(NSURLQueryItem(name:key,value: "\(value)"))
            }
            components.queryItems = queryItems as [URLQueryItem]?
            return components.url as NSURL?
        }
        return nil
    }
    
    func POSTURLforRoute(route:String) -> URL?{
        
        if let components: NSURLComponents = NSURLComponents(string: (Constants.BaseURL+route)){
            return components.url! as URL
        }
        return nil
    }
    
}

//MARK:- Response Handle
extension APIManagerBase{
    fileprivate func responseResult(_ response:DataResponse<Any>,
                                    success: @escaping (_ response: AnyObject) -> Void,
                                    failure: @escaping (_ error: NSError) -> Void) {
      //  Utility.hideLoader()
//        Utility.main.hideSpinner()
        let errorGenericMessage = Strings.ERROR_GENERIC_MESSAGE.text + "\(response.response?.url?.absoluteString ?? "")"
        switch response.result{
        case .success:
            if let dictData = response.result.value as? NSDictionary {
//                if let message = dictData["message"] as? String{
//                    Constants.apiMessage = message}
//                if let status = dictData["status"] as? Int{
//                    if status == 201{
//                        if Route.PushNotificationFirebase.rawValue == Global.PushNotificationFirebase {
//                            success(dictData["message"] as AnyObject)
//                            Global.PushNotificationFirebase = ""
//                        }
//                    }
//                    if status == 200 {
//                        //Just to handle  response from add to cart and delete cart api
//                        if Route.AddToCartItem.rawValue == Global.addToCart || Route.DeleteItemFromCart.rawValue == Global.deleteFromCart || Global.forgetPassword == Route.ForgetPassword.rawValue || Global.changePassword == Route.ChangePassword.rawValue {
//                            success(dictData["message"] as AnyObject)
//                            Global.addToCart = ""
//                            Global.deleteFromCart = ""
//                            Global.forgetPassword = ""
//                            Global.changePassword = ""
//                            return
//                        }
//                        else{
//                            success(dictData["data"] as AnyObject)
//                            return
//                        }
//                    }
//                    else{
//                        if let error = (dictData["message"] as? String){
//                            // show error only when sign up normally
//                            if Global.flagSignUp {
//                                Utility.main.showToast(message: error.description)
//                                Global.flagSignUp = false
//                            }
//                            if let requestURL = response.request?.url {
//                                if requestURL.absoluteString.contains(Route.UserSignUp.rawValue){
//                                    return
//                                }
//                            }
//
//                            Utility.main.showToast(message: error)
//                            Global.addToCart = ""
//                            Global.deleteFromCart = ""
//                            Global.forgetPassword = ""
//                            Global.changePassword = ""
//                            return
//                        }
//                        else{
//                            Utility.main.showAlert(message: errorGenericMessage, title: Strings.ERROR.text)
//                            Global.addToCart = ""
//                            Global.deleteFromCart = ""
//                            Global.forgetPassword = ""
//                            Global.changePassword = ""
//                            return
//                        }
//                    }
//                }
                if let error = (dictData["error_description"] as? String), !error.isEmpty{
                    
                    let userInfo = [NSLocalizedFailureReasonErrorKey: error]
                    let error = NSError(domain: "Domain", code: 0, userInfo: userInfo);
                    failure(error)
                    return
                }
                else{
                    success(dictData)
                    return
                }
            }
            //just handle cuisine api response
            else if let dictData = response.result.value as? NSArray {
                success(dictData)
                return
                
            }else if let cartCount = response.result.value as? Int, cartCount>=0{
                Global.cartCount = cartCount
                success(cartCount as AnyObject)
                return
            }
            else {
                //Failure
                let errorMessage: String = Strings.UNKNOWN_ERROR.text + "\(response.response?.url?.absoluteString ?? "")"
                let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                let error = NSError(domain: "Domain", code: 0, userInfo: userInfo);
                failure(error)
            }
        case .failure(let error):
            failure(error as NSError)
            Utility.main.showToast(message: error.localizedDescription)
            //Utility.main.showAlert(message: errorGenericMessage, title: Strings.ERROR.text)
        }
    }
}

//MARK:- Get APIs
extension APIManagerBase{
    
    func getArrayResponseWith(route: URL,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure,
                        withHeader: Bool){
        if withHeader{
            Alamofire.request(route, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {
                    response in
                    if let arrayResponse = response as? Array<AnyObject>{
                        success(arrayResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })}
        }
        else{
            Alamofire.request(route, method: .get, parameters: nil, encoding: URLEncoding()).responseJSON {
                response in
                
                self.responseResult(response, success: {response in
                    if let arrayResponse = response as? Array<AnyObject>{
                        success(arrayResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
    func getDictionaryResponseWith(route: URL,
                                     success:@escaping DefaultAPISuccessClosure,
                                     failure:@escaping DefaultAPIFailureClosure, withHeader: Bool){
        if withHeader{
            Alamofire.request(route, method: .get, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                print("response:-> \(response)")
                self.responseResult(response, success: {response in
                    if let dictionaryResponse = response as? Dictionary<String, AnyObject> {
                        success(dictionaryResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
        else{
            Alamofire.request(route, method: .get, parameters: nil, encoding: URLEncoding()).responseJSON {
                response in
                self.responseResult(response, success: {response in
                    if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                        success(dictionaryResponse)
                    }
                    else{
                        success(Dictionary<String, AnyObject>())
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
    func getBoolResponseWith(route: URL,
                               success:@escaping DefaultBoolResultAPISuccesClosure,
                               failure:@escaping DefaultAPIFailureClosure, withHeader: Bool){
        if withHeader{
            Alamofire.request(route, method: .get, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    success(true)
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
        else{
            Alamofire.request(route, method: .get , parameters: nil, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    success(true)
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }

    func getStringResponseWith(route: URL,
                                 success:@escaping DefaultStringResultAPISuccesClosure,
                                 failure:@escaping DefaultAPIFailureClosure, withHeader: Bool){
        if withHeader{
            Alamofire.request(route, method: .get, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let stringResponse = response as? String{
                        success(stringResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
        else{
            Alamofire.request(route, method: .get , parameters: nil, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let stringResponse = response as? String{
                        success(stringResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }

    func getIntResponseWith(route: URL,
                             success:@escaping DefaultIntResultAPISuccesClosure,
                             failure:@escaping DefaultAPIFailureClosure, withHeader: Bool){
        if withHeader{
            Alamofire.request(route, method: .get, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let intResponse = response as? Int{
                        success(intResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
        else{
            Alamofire.request(route, method: .get , parameters: nil, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let intResponse = response as? Int{
                        success(intResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
}

//MARK:- Post APIs
extension APIManagerBase{
    
    func postArrayResponseWith(route: URL,parameters: Parameters,
                              success:@escaping DefaultArrayResultAPISuccessClosure,
                              failure:@escaping DefaultAPIFailureClosure, withHeaders:Bool){
        
        if withHeaders {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let arrayResponse = response as? Array<AnyObject>{
                        success(arrayResponse)
                    }
                    else{
                        success(Array<AnyObject>())
                    }
                }, failure: {error in
                    
                    failure(error as NSError)
                })
            }
        }else {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let arrayResponse = response as? Array<AnyObject>{
                        success(arrayResponse)
                    }
                    else{
                        success(Array<AnyObject>())
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
    func postDictionaryResponseWith(route: URL,parameters: Parameters,
                         success:@escaping DefaultAPISuccessClosure,
                         failure:@escaping DefaultAPIFailureClosure, withHeader:Bool){
        
        if withHeader{
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                        success(dictionaryResponse)
                    }
                    else{
                        success(Dictionary<String, AnyObject>())
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }else {    // EDIT ELSE PART FOR FORGET PASSWORD API -> DICTIONARY<STRING:ANYOBJECT> TO ANYOBJECT ONLY.
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                        success(dictionaryResponse)
                    }
                    else{
                        success(Dictionary<String, AnyObject>())
//                        success(response as? AnyObject  Dictionary<String, AnyObject>)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
//    MARK:-  SOCIAL SIGNUP RESPONSE
    
    func postDictionaryResponseWithSocialLogin(route: URL,parameters: Parameters,
                         success:@escaping DefaultAPISuccessClosure,
                         failure:@escaping DefaultAPIFailureClosure, withHeader:Bool){
        
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                        success(dictionaryResponse)
                    }
                    else{
                        success(Dictionary<String, AnyObject>())
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        success(Dictionary<String, AnyObject>())
        }
    
    
    func postDictionaryResponseForLoginWith(route: URL,
                         success:@escaping DefaultArrayResultAPISuccessClosure,
                         failure:@escaping DefaultAPIFailureClosure, withHeader:Bool){
        let user = "ck_53de6fc509778ff6ee02f8b2ce904c70b50cf592"
        let password = "cs_2f1eb8af34656d32ca3c87c48c0180b4f41be45b"
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)
        let base64Credentials = credentialData?.base64EncodedString(options: []) ?? ""
        let headers = ["Authorization": "oAuth 1.0 \(base64Credentials)"]
        
        Alamofire.request(route, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON{
            response in
            self.responseResult(response, success: {response in
                if let dictionaryResponse = response as? Array<AnyObject>{
                    success(dictionaryResponse)
                }
                else{
                    success(Array<AnyObject>())
                    //Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                }
            }, failure: {error in
                failure(error as NSError)
            })
        }
    }
    
    func postDictionaryResponseWithURLEncoding(route: URL,parameters: Parameters,
                                    success:@escaping DefaultAPISuccessClosure,
                                    failure:@escaping DefaultAPIFailureClosure, withHeader:Bool){
        
        if withHeader{
            Alamofire.request(route, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                        success(dictionaryResponse)
                    }
                    else{
                        success(Dictionary<String, AnyObject>())
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }else {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                        success(dictionaryResponse)
                    }
                    else{
                        success(Dictionary<String, AnyObject>())
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
    func postMultipartDictionaryResponseWith(route: URL,parameters: Parameters,
                                  success:@escaping DefaultAPISuccessClosure,
                                  failure:@escaping DefaultAPIFailureClosure , withHeader: Bool){
        if withHeader{
            Alamofire.upload(multipartFormData:{ multipartFormData in
                for (key , value) in parameters {
                    if let data:Data = value as? Data {
                        let file_key = key.components(separatedBy: CharacterSet.decimalDigits).joined()
                        multipartFormData.append(data, withName: file_key, fileName: "\(data.getExtension)", mimeType: "\(data.mimeType)")
                    } else {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                    }
                    print(multipartFormData)
                }
            },
                             usingThreshold:UInt64.init(),
                             to: route,
                             method:.post,
                             headers: getAuthorizationHeader(),
                             encodingCompletion: { result in
                                switch result {
                                case .success(let upload, _, _):
                                    upload.responseJSON { response in
                                        self.responseResult(response, success: {result in
                                            if let dictionaryResponse = result as? Dictionary<String, AnyObject>{
                                                success(dictionaryResponse)
                                            }
                                            else{
                                                success(Dictionary<String, AnyObject>())
                                                //Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                                            }
                                        }, failure: {error in
                                            
                                            failure(error)
                                        })
                                    }
                                case .failure(let encodingError):
                                    failure(encodingError as NSError)
                                }
            }
                
            )
            
            
        }else{
            Alamofire.upload (
                multipartFormData: { multipartFormData in
                    for (key , value) in parameters {
                        if let data:Data = value as? Data {
                            multipartFormData.append(data, withName: key, fileName: "\(data.getExtension)", mimeType: "\(data.mimeType)")
                        } else {
                            multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                        }
                    }
                    
            },
                to: route,
                encodingCompletion: { result in
                    switch result {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            self.responseResult(response, success: {result in
                                if let dictionaryResponse = result as? Dictionary<String, AnyObject>{
                                    success(dictionaryResponse)
                                }
                                else{
                                    Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                                }
                            }, failure: {error in
                                
                                failure(error)
                            })
                        }
                    case .failure(let encodingError):
                        failure(encodingError as NSError)
                    }
            }
            )
        }
    }

    func postBoolResponseWith(route: URL,parameters: Parameters,
                                success:@escaping DefaultBoolResultAPISuccesClosure,
                                failure:@escaping DefaultAPIFailureClosure, withHeader:Bool){
        
        if withHeader{
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    success(true)
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }else {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    success(true)
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
    func postVoidResponseWith(route: URL,parameters: Parameters,
                              success:@escaping DefaultVoidAPIResponse,
                              failure:@escaping DefaultAPIFailureClosure, withHeader: Bool){
        if withHeader{
            Alamofire.request(route,method: .post, parameters: parameters,encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                print("response",response)
//                Utility.main.hideSpinner()
            }
        }else{
            Alamofire.request(route, method: .post , parameters: nil, encoding: JSONEncoding.default).responseJSON{
                response in
                print("response",response)
//                Utility.main.hideSpinner()
            }
        }
    }

    func postStringResponseWith(route: URL,parameters: Parameters,
                               success:@escaping DefaultStringResultAPISuccesClosure,
                               failure:@escaping DefaultAPIFailureClosure, withHeader: Bool){
        if withHeader{
            Alamofire.request(route, method: .post,parameters: parameters, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let stringResponse = response as? String{
                        success(stringResponse)
                        // yahan krna hy kuch...
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
        else{
            Alamofire.request(route, method: .post , parameters: nil, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let stringResponse = response as? String{
                        success(stringResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
    func postIntResponseWith(route: URL,
                               success:@escaping DefaultIntResultAPISuccesClosure,
                               failure:@escaping DefaultAPIFailureClosure, withHeader: Bool){
        if withHeader{
            Alamofire.request(route, method: .post, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let intResponse = response as? Int{
                        success(intResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
        else{
            Alamofire.request(route, method: .post , parameters: nil, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let intResponse = response as? Int{
                        success(intResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
}

//MARK:- Delete APIs
extension APIManagerBase{
    func deleteDictionaryResponseWith(route: URL,parameters: Parameters,
                                    success:@escaping DefaultAPISuccessClosure,
                                    failure:@escaping DefaultAPIFailureClosure, withHeader:Bool){
        
        if withHeader{
            Alamofire.request(route, method: .delete, parameters: parameters, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                        success(dictionaryResponse)
                    }
                    else{
                        success(Dictionary<String, AnyObject>())
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }else {
            Alamofire.request(route, method: .delete, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                        success(dictionaryResponse)
                    }
                    else{
                        success(Dictionary<String, AnyObject>())
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
}


//MARK:- Put APIs
extension APIManagerBase{
    
    func putDictionaryResponseWith(route: URL,parameters: Parameters,
                                    success:@escaping DefaultAPISuccessClosure,
                                    failure:@escaping DefaultAPIFailureClosure, withHeader:Bool){
        
        if withHeader{
            Alamofire.request(route, method: .put, parameters: parameters, encoding: URLEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                        success(dictionaryResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }else {
            Alamofire.request(route, method: .put, parameters: parameters, encoding: URLEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                        success(dictionaryResponse)
                    }
                    else{
                        success(Dictionary<String, AnyObject>())
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
    func putMultipartDictionaryResponseWith(route: URL,parameters: Parameters,
                                             success:@escaping DefaultAPISuccessClosure,
                                             failure:@escaping DefaultAPIFailureClosure , withHeader: Bool){
        if withHeader{
            Alamofire.upload(multipartFormData:{ multipartFormData in
                for (key , value) in parameters {
                    if let data:Data = value as? Data {
                        multipartFormData.append(data, withName: key, fileName: "\(data.getExtension)", mimeType: "\(data.mimeType)")
                    } else {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                    }
                }
            },
                             usingThreshold:UInt64.init(),
                             to: route,
                             method:.put,
                             headers: getAuthorizationHeader(),
                             encodingCompletion: { result in
                                switch result {
                                case .success(let upload, _, _):
                                    upload.responseJSON { response in
                                        self.responseResult(response, success: {result in
                                            if let dictionaryResponse = result as? Dictionary<String, AnyObject>{
                                                success(dictionaryResponse)
                                            }
                                            else{
                                                Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                                            }
                                        }, failure: {error in
                                            
                                            failure(error)
                                        })
                                    }
                                case .failure(let encodingError):
                                    failure(encodingError as NSError)
                                }
            }
                
            )
            
            
        }else{
            Alamofire.upload (
                multipartFormData: { multipartFormData in
                    for (key , value) in parameters {
                        if let data:Data = value as? Data {
                            multipartFormData.append(data, withName: key, fileName: "\(data.getExtension)", mimeType: "\(data.mimeType)")
                        } else {
                            multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                        }
                    }
                    
            },
                to: route,
                encodingCompletion: { result in
                    switch result {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            self.responseResult(response, success: {result in
                                if let dictionaryResponse = result as? Dictionary<String, AnyObject>{
                                    success(dictionaryResponse)
                                }
                                else{
                                    Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                                }
                            }, failure: {error in
                                
                                failure(error)
                            })
                        }
                    case .failure(let encodingError):
                        failure(encodingError as NSError)
                    }
            }
            )
        }
    }
}

public extension Data {
    var mimeType:String {
    get {
        var c = [UInt32](repeating: 0, count: 1)
        (self as NSData).getBytes(&c, length: 1)
        switch (c[0]) {
        case 0xFF:
            return "image/jpeg";
        case 0x89:
            return "image/png";
        case 0x47:
            return "image/gif";
        case 0x49, 0x4D:
            return "image/tiff";
        case 0x25:
            return "application/pdf";
        case 0xD0:
            return "application/vnd";
        case 0x46:
            return "text/plain";
        default:
            print("mimeType for \(c[0]) in available");
            return "application/octet-stream";
        }
    }
}
    var getExtension: String {
    get {
        var c = [UInt32](repeating: 0, count: 1)
        (self as NSData).getBytes(&c, length: 1)
        switch (c[0]) {
        case 0xFF:
            return "_IMG.jpeg";
        case 0x89:
            return "_IMG.png";
        case 0x47:
            return "_IMG.gif";
        case 0x49, 0x4D:
            return "_IMG.tiff";
        case 0x25:
            return "_FILE.pdf";
        case 0xD0:
            return "_FILE.vnd";
        case 0x46:
            return "_FILE.txt";
        default:
            print("mimeType for \(c[0]) in available");
            return "_video.mp4";
        }
    }
}
}
