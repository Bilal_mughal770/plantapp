//
//  UsersAPIManager.swift
//  S2OCustomer
//
//  Created by Fazeel Ahmed on 15/04/2021.
//  Copyright © 2021 Mustafa Siddique. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class UsersAPIManager : APIManagerBase {
    
    //MARK:- POST: LOGIN
    func LoginUser(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route: URL = POSTURLforRoute(route: Route.Login.rawValue)! as URL
        print(route)
        self.postDictionaryResponseWith(route: route, parameters: params, success: success, failure: failure, withHeader: false)
        
    }
    
    //    MARK:- GET:  FORGET PASSWORD
    func forgetPassword(param: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let email = param["email"] as! String
        let route: URL = POSTURLforRoute(route: Route.ForgetPassword.rawValue + email)! as URL
        print(route)
        self.getDictionaryResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
    
    //    MARK:- GET:  STRAIN LIST
    func getStrainList(success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        
        let route: URL = POSTURLforRoute(route: Route.strainList.rawValue)! as URL
        print(route)
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
    
    //    MARK:- GET:  DISCOVER MENU
    func getDiscoverMenu(success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        
        let route: URL = POSTURLforRoute(route: Route.discoverMenu.rawValue)! as URL
        print(route)
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
    
    //    MARK:- GET:  TAXES
    func getTaxes(success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
//        let
        let route: URL = POSTURLforRoute(route: Route.taxes.rawValue)! as URL
        print(route)
//        self.postDictionaryResponseForLoginWith(route: route, success: success, failure: failure, withHeader: false)
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: false)
        
    }
    
    //    MARK:- GET:  Video List
    func getVideoList( success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {

        let route: URL = POSTURLforRoute(route: Route.training_New_Single_VideoList.rawValue)! as URL
        print(route)
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
    
    //    MARK:- GET:  Training Video List
    func getTrainingVideoList(param: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {

        let route: URL = URLforRoute(route: Route.training_New_Single_VideoList.rawValue, params: param)! as URL
        print(route)
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
    
    //    MARK:- GET:  New Video List
    func getNewVideoList(param: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {

        let route: URL = URLforRoute(route: Route.training_New_Single_VideoList.rawValue, params: param)! as URL
        print(route)
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
    
    //    MARK:- GET:  single video Detail
    func getSingleVideoDetail(param: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let id = param["id"] as! String
        let route: URL = POSTURLforRoute(route: Route.training_New_Single_VideoList.rawValue + id )! as URL
        print(route)
        self.getDictionaryResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
   
    //    MARK:- GET:  PLANT BY TYPES
    func getPlantByTypes( success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {

        let route: URL = POSTURLforRoute(route: Route.plantByTypes.rawValue )! as URL
        print(route)
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
    
    //    MARK:- GET:  PLANT BY NIGHT TYPES
    func getPlantByNight( success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {

        let route: URL = POSTURLforRoute(route: Route.plantByNightTime.rawValue )! as URL
        print(route)
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
    
    //    MARK:- GET:  POPULAR PLANT
    func getPopularPlant( success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {

        let route: URL = POSTURLforRoute(route: Route.plantByPopular.rawValue )! as URL
        print(route)
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
    
    //    MARK:- GET:  POSTS
    func getPosts( success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {

        let route: URL = POSTURLforRoute(route: Route.posts_categoryWise.rawValue )! as URL
        print(route)
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
    //    MARK:- GET:  POSTS CATEGORIES
    func getPostsCategories( success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {

        let route: URL = POSTURLforRoute(route: Route.postsCategories.rawValue )! as URL
        print(route)
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
    
    //    MARK:- GET:  CATEGORY WISE POSTS
    func getCatagoryWisePosts( param: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {

        let route: URL = URLforRoute(route: Route.posts_categoryWise.rawValue, params: param )! as URL
        print(route)
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
    
    //    MARK:- GET:  POST COMMENTS
    func getPostComments( success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {

        let route: URL = POSTURLforRoute(route: Route.postComment_CustomComments.rawValue )! as URL
        print(route)
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
    
    //    MARK:- GET:  CUSTOM POST COMMENTS
    func getCustomPostComments( param: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {

        let route: URL = URLforRoute(route: Route.postComment_CustomComments.rawValue, params: param )! as URL
        print(route)
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
    
    
    
    
    
    
    //---------------------------------------------
    
    //    MARK:- POST: SIGNUP
    func signUp(param: Parameters, success: @escaping DefaultAPISuccessClosure,failure: @escaping DefaultAPIFailureClosure){
        let route: URL = POSTURLforRoute(route: Route.UserSignUp.rawValue)! as URL
        print(route)
//        self.getDictionaryResponseWith(route: route, success: success, failure: failure, withHeader: false)
        self.postDictionaryResponseWith(route: route, parameters: param, success: success, failure: failure, withHeader: false)
    }
    //    MARK:- POST: SOCIAL SIGNUP
    func socialSignUp(param: Parameters, success: @escaping DefaultAPISuccessClosure,failure: @escaping DefaultAPIFailureClosure){
        let route: URL = POSTURLforRoute(route: Route.UserSignUp.rawValue)! as URL
        print(route)
        self.postDictionaryResponseWithSocialLogin(route: route, parameters: param, success: success, failure: failure, withHeader: false)
    }
    
    //MARK:- GET: CUISINES DATA
    func GetAllCuisine(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route: URL = POSTURLforRoute(route: Route.Cuisines.rawValue)! as URL
        print(route)
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: true)
        
    }
    
    //MARK:- GET: MERCHANT RESTAURANTS
    func GetMerchantsRestaurants(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.MerchantRestaurants.rawValue, params: params)! as URL
        print(route)
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: true)
        
    }
    
    //    MARK:-  GET: ORDER DETAILS
    
    func GetOrderDetails(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.OrderDetails.rawValue, params: params)! as URL
        print(route)
        self.getDictionaryResponseWith(route: route, success: success, failure: failure, withHeader: true)
        
    }
    
    //    MARK:-  GET FILTERS
    
    func GetFilters(success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route: URL = POSTURLforRoute(route: Route.Filters.rawValue)! as URL
        print(route)
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: true)
        
    }
    
    //    MARK:-  APPLY GET FILTERS DATA
    
    func GetApplyFiltersData(params : Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.ApplyFilters.rawValue, params: params)! as URL
        print(route)
        self.postArrayResponseWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
        
    }
    

    
    //    MARK:- GET: RESTAURANT ITEMS
    
    func getRestaurantItems(param: Parameters,success: @escaping DefaultArrayResultAPISuccessClosure,failure: @escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.RestaurantItems.rawValue, params: param)! as URL
        print(route)
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    //    MARK:- GET: CART ITEMS
    
    func getCartItems(success: @escaping DefaultArrayResultAPISuccessClosure,failure: @escaping DefaultAPIFailureClosure){
        let route: URL = POSTURLforRoute(route: Route.CartItems.rawValue)! as URL
        print(route)
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    //    MARK:- GET: PAYMENT DETAILS
    
    func getPaymentDetails(param: Parameters,success: @escaping DefaultAPISuccessClosure,failure: @escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.PaymentDetails.rawValue, params: param)! as URL
        print(route)
        self.getDictionaryResponseWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    //    MARK:- POST: ADD ITEM TO CART
    
    func addItemToCart(param: Parameters,success: @escaping DefaultAPISuccessClosure,failure: @escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.AddToCartItem.rawValue, params: param)! as URL
        Global.addToCart = Route.AddToCartItem.rawValue
        print(route)
        self.postDictionaryResponseWith(route: route, parameters: param, success: success, failure: failure, withHeader: true)
    }
    
    //    MARK:- DELETE: DELETE ITEM TO CART
    
    func deleteItemFromCart(param: Parameters,success: @escaping DefaultAPISuccessClosure,failure: @escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.DeleteItemFromCart.rawValue, params: param)! as URL
        Global.deleteFromCart = Route.DeleteItemFromCart.rawValue
        print(route)
        self.deleteDictionaryResponseWith(route: route, parameters: param, success: success, failure: failure, withHeader: true)
    }
    
    //    MARK:- GET: CART COUNT
    
    func getCartCount(success: @escaping DefaultIntResultAPISuccesClosure,failure: @escaping DefaultAPIFailureClosure){
        let route: URL = POSTURLforRoute(route: Route.CartCount.rawValue)! as URL
        
        print(route)
        self.getIntResponseWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    
    //    MARK:- GET: MAP VIEW DATA
    
    func getMapViewData(param: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure,failure: @escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.MapViewData.rawValue, params: param)! as URL
        print(route)
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    //MARK:- GET: STATES DATA
    func getAllSates(success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route: URL = POSTURLforRoute(route: Route.States.rawValue)! as URL
        print(route)
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: false)
        
    }
    
    //MARK:- GET: CITIES DATA
    func getAllCitiesByStateId(param: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.Cities.rawValue, params: param)! as URL
        print(route)
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: false)
        
    }
    
    //    MARK:-  GET: USER ACCOUNT DETAILS
    
    func getUserAccountDetails(success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route: URL = POSTURLforRoute(route: Route.UserAccountDetails.rawValue)! as URL
        print(route)
        self.getDictionaryResponseWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    //MARK:- POST: UPDATE USER
    

//        func updateUser(param: Parameters,success: @escaping DefaultAPISuccessClosure,failure: @escaping DefaultAPIFailureClosure){
//            let route: URL = POSTURLforRoute(route: Route.UpdateUser.rawValue)! as URL
//            print(route)
//            self.postMultipartDictionaryResponseWith(route: route, parameters: param, success: success, failure: failure, withHeader: true)
//        }

    
    //    MARK:- POST: ADD REFERRALS EMAILs
    
    func addReferrals(param: Parameters,success: @escaping DefaultStringResultAPISuccesClosure,failure: @escaping DefaultAPIFailureClosure){
        let route: URL = POSTURLforRoute(route: Route.AddReferrals.rawValue)! as URL
        print(route)
        self.postStringResponseWith(route: route, parameters: param, success: success, failure: failure, withHeader: true)
    }
    
    //    MARK:- POST: CHANGE PASSWORD
    
    func changePassword(param: Parameters,success: @escaping DefaultStringResultAPISuccesClosure,failure: @escaping DefaultAPIFailureClosure){
        let route: URL = POSTURLforRoute(route: Route.ChangePassword.rawValue)! as URL
        print(route)
        Global.changePassword = Route.ChangePassword.rawValue
        self.postStringResponseWith(route: route, parameters: param, success: success, failure: failure, withHeader: true)
    }
    
    //    MARK:-  GET: CHECK EMAIL VALID
    
    func getEmailValidity(param: Parameters,success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.CheckEmailValid.rawValue, params: param)! as URL
        print(route)
        self.getDictionaryResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
    //    MARK:- GET: ORDERS
    
    func getOrders(param: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure,failure: @escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.Orders.rawValue, params: param)! as URL
        print(route)
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    
    //MARK:- GET: SEARCHING DATA
    func getSearchingItems(param: Parameters,success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.SearchingItems.rawValue, params: param)! as URL
        print(route)
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: true)
        
    }
    
    //    MARK:- GET: SEARCHED ITEM BY ID
    
    func getSearchedItemById(param: Parameters,success: @escaping DefaultAPISuccessClosure,failure: @escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.SearchingItemFindById.rawValue, params: param)! as URL
        print(route)
        self.getDictionaryResponseWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    //    MARK:- POST: PAYMENT CREDIT CARD WITH NEW CARD
    
    func paymentWithNewCard(param: Parameters,success: @escaping DefaultAPISuccessClosure,failure: @escaping DefaultAPIFailureClosure){
        let route: URL = POSTURLforRoute(route: Route.PaymentWithNewCard.rawValue)! as URL
        print(route)
        self.postDictionaryResponseWith(route: route, parameters: param, success: success, failure: failure, withHeader: true)
    }
    
    //    MARK:- POST: PAYMENT CREDIT CARD WITH NEW CARD
    
    func deleteFirebaseToken(success: @escaping DefaultVoidAPIResponse,failure: @escaping DefaultAPIFailureClosure){
        let route: URL = POSTURLforRoute(route: Route.deleteFireBaseToken.rawValue)! as URL
        print(route)
        self.postVoidResponseWith(route: route, parameters: [:], success: success, failure: failure, withHeader: true)
    }
//    MARK:- POST: PAYPAL INTEGRATION
    
    func paymentWithPaypal(param: Parameters,success: @escaping DefaultAPISuccessClosure,failure: @escaping DefaultAPIFailureClosure){
        let route: URL = POSTURLforRoute(route: Route.PaymentWithPaypal.rawValue)! as URL
        print(route)
        self.postDictionaryResponseWith(route: route, parameters: param, success: success, failure: failure, withHeader: true)
    }
    
//    MARK:- POST: PUSH NOTIFICATION FIREBASE
    
    func addFirebaseTokenFCM(param: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Route.PushNotificationFirebase.rawValue, params: param)! as URL
        Global.PushNotificationFirebase = Route.PushNotificationFirebase.rawValue
        print("pushNotification",route)
        self.postDictionaryResponseWith(route: route, parameters: [:], success: success, failure: failure, withHeader: true)
    }
    
    //    MARK:- POST: PAYPAL PLACE ORDER
        
    func paypalPlaceOrder(paypalId id: Parameters, param: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
            let route: URL = URLforRoute(route: Route.PaypalPlaceOrder.rawValue, params: id)! as URL
            print(route)
        self.postDictionaryResponseWith(route: route, parameters: param, success: success, failure: failure, withHeader: true)
        }
    //    MARK:- POST: SQUARE NEW CREDIT CARD PLACE ORDER
        
    func squareNewCreditCardPlaceOrder(nonce token: Parameters, param: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
            let route: URL = URLforRoute(route: Route.SquareNewCreditCard.rawValue, params: token)! as URL
            print(route)
        self.postDictionaryResponseWith(route: route, parameters: param, success: success, failure: failure, withHeader: true)
        }
    //    MARK:- GET: SQUARE EXISTING CREDIT CARD FIRST
    
    func getSquareExistingCreditCard(success: @escaping DefaultAPISuccessClosure,failure: @escaping DefaultAPIFailureClosure){
        let route: URL = POSTURLforRoute(route: Route.SquareBeforeExistingCreditCard.rawValue)! as URL
        print(route)
        self.getDictionaryResponseWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //    MARK:- POST: SQUARE EXISTING CREDIT CARD PLACE ORDER
        
    func squareExistingCreditCardPlaceOrder(param: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
            let route: URL = POSTURLforRoute(route: Route.SquareExistingCreditCard.rawValue)! as URL
            print(route)
        self.postDictionaryResponseWith(route: route, parameters: param, success: success, failure: failure, withHeader: true)
        }
}



