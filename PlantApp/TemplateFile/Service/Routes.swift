//
//  APIManagerBase.swift
//  S2OCustomer
//
//  Created by Fazeel Ahmed on 15/04/2021.
//  Copyright © 2021 Mustafa Siddique. All rights reserved.
//

import Foundation


enum Route: String {
    //MARK:- USER
    
//    registration api : then login api then fcm wali api...
    
    case Login = "customer/login"
    case ForgetPassword = "apiuser/v1/forgotpwd"
    case strainList = "strain/strain-list"
    case discoverMenu = "discover/discover-menu"
    case taxes = "wc/v3/taxes"
    case training_New_Single_VideoList = "wp/v2/video"  // for 4 end points
    case singleVideoDetails = "wp/v2/video/40197"
    case plantByTypes = "product/product-list-count"
    case plantByNightTime = "products/night-time-usage"
    case plantByPopular = "popular/products"
    case posts_categoryWise = "wp/v2/posts"
    case postsCategories = "wp/v2/categories"
    case postComment_CustomComments = "wp/v2/comments"
    
    
    
    
    
    
    
    
    
    case Cuisines = "cuisine/get/all"
    case MerchantRestaurants = "merchant/get/merchant/on/cuisine"
    case OrderDetails = "order/get/order/details"
    case Filters = "filter/"
    case ApplyFilters = "filter/get-merchants-by-cuisine-id-list"
    case CheckEmailValid = "changePassword/EmailValidity"
    case RestaurantItems = "item/find/merchant"
    case CartItems = "cart/get/all"
    case PaymentDetails = "merchant/details/for/checkout"
    case AddToCartItem = "cart/add"
    case DeleteItemFromCart = "cart/delete"
    case CartCount = "cart/get/count"
    case MapViewData = "merchant/city/name/list/restaurant"
    case UserSignUp = "auth/register"
    case States = "state/get/all"
    case Cities = "city/get/by/state"
    case UserAccountDetails = "api/v2.0/user/paymentdetails"
    case AddReferrals = "referral/invite"
    case ChangePassword = "changePassword/change"
    case Orders = "order/get/all"
    case UpdateUser = "api/v2.0/user/UpdateUser"
    case PaymentWithNewCard = "/api/1.0/square/new-card"
    case SearchingItems = "search/dropdown"
    case SearchingItemFindById = "item/find"
    case PushNotificationFirebase = "auth/register-firebase-user-token"
    case PaymentWithPaypal = "api/1.0/paypal/process"
    case PaypalPlaceOrder  = "api/1.0/paypal/place-order"
    case SquareNewCreditCard = "api/1.0/square/new-card"
    case SquareExistingCreditCard = "api/1.0/square/with-existing-card"
    case SquareBeforeExistingCreditCard = "api/1.0/square/"
    case deleteFireBaseToken = "auth/delete-firebase-user-token"
    //MARK:- GENERAL


 
    
}
