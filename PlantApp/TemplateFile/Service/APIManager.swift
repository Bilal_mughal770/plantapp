//
//  APIManager.swift
//  S2OCustomer
//
//  Created by Fazeel Ahmed on 15/04/2021.
//  Copyright © 2021 Mustafa Siddique. All rights reserved.
//

import Foundation
import UIKit

class APIManager: NSObject {
    
    static let sharedInstance = APIManager()
    
    var serverToken: String? {
        get{
            if AppStateManager.sharedInstance.isUserLoggedIn(){
                return AppStateManager.sharedInstance.loggedInUser.data?.userPass ?? ""
            }
            return Constants.accessToken
        }
    }
    
    let usersAPIManager = UsersAPIManager()
}
typealias DefaultAPISuccessClosure = (Dictionary<String,AnyObject>) -> Void
typealias DefaultArrayResultAPISuccessClosure = (Array<AnyObject>) -> Void
typealias DefaultStringResultAPISuccesClosure = (String) -> Void
typealias DefaultIntResultAPISuccesClosure = (Int) -> Void
typealias DefaultBoolResultAPISuccesClosure = (Bool) -> Void
typealias DefaultAPIFailureClosure = (NSError) -> Void
typealias DefaultImageResultClosure = (UIImage) -> Void
typealias DefaultDownloadSuccessClosure = (Data) -> Void
typealias DefaultDownloadProgressClosure = (Double, Int) -> Void
typealias DefaultDownloadFailureClosure = (NSError, Data, Bool) -> Void
typealias DefaultVoidAPIResponse = () -> Void

protocol APIErrorHandler {
    func handleErrorFromResponse(response: Dictionary<String,AnyObject>)
    func handleErrorFromERror(error:NSError)
}


