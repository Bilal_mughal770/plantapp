//
//  AppStateManager.swift
//  S2OCustomer
//
//  Created by Fazeel Ahmed on 15/04/2021.
//  Copyright © 2021 Mustafa Siddique. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class AppStateManager: NSObject {
    
    static let sharedInstance = AppStateManager()
    var loggedInUser: UserModel!
    var realm: Realm!
    
    override init() {
        super.init()
        if(!(realm != nil)){
            realm = try! Realm()
        }
        loggedInUser = realm.objects(UserModel.self).first
    }
    func isUserLoggedIn() -> Bool{
        if (self.loggedInUser) != nil {
            if self.loggedInUser.isInvalidated {
                return false
            }
            return true
        }
        return false
    }
    func saveUser(user:UserModel) {
        try! Global.APP_REALM?.write(){
            AppStateManager.sharedInstance.loggedInUser = user
            Global.APP_REALM?.add(user, update: .all)
            
            //            AppDelegate.shared.registerDeviceToken()
        }
    }
    func updateUser(user:UserModel) {
        try! Global.APP_REALM?.write(){
            AppStateManager.sharedInstance.loggedInUser = user
            Global.APP_REALM?.add(user, update: .all)
        }
    }
    func loginUser(user:UserModel) {
        try! Global.APP_REALM?.write(){
            AppStateManager.sharedInstance.loggedInUser = user
            Global.APP_REALM?.add(user, update: .all)
            
            //            AppDelegate.shared.registerDeviceToken()
        }
//        AppDelegate.shared.changeRootViewController()
        
        let loggedInUserID = Constants.USER_DEFAULTS.value(forKey: "userID") as? Int
        if loggedInUserID == nil{
            //  let userID = user.iD
            let userID = ""
            Constants.USER_DEFAULTS.set(userID, forKey: "userID")
        }
    }
    func logoutUser(){
        DispatchQueue.main.async {
            Utility.main.showAlert(message: Strings.ASK_LOGOUT.text, title: Strings.LOGOUT.text, controller: Utility.main.topViewController()!) { (yes, no) in
                if yes != nil{
                    self.processLogoutUser()
                }
            }
        }
    }
}
extension AppStateManager{
    func processLogoutUser(){

            try! Global.APP_REALM?.write() {
                Global.APP_REALM?.delete(self.loggedInUser)
                self.loggedInUser = nil
            
           AppDelegate.shared.changeRootViewController()

        }
    }
}

