//
//  Constants.swift
//  PlantApp
//
//  Created by Nayyer Ali on 22/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import Foundation
import SCLAlertView

let darkThemeOn  =  Notification.Name(rawValue: "darkThemeOn")
let darkThemeOff  =  Notification.Name(rawValue: "darkThemeOff")
let isDarkModeOn = "isDarkModeOn"
let ok = "OK"
let alertIcon = UIImage(named: "Logo")!
let alertAppearence = SCLAlertView.SCLAppearance(kCircleIconHeight: 40)
let apppThemeColor = UIColor(red: 0/255, green: 203/255, blue: 117/255, alpha: 1)


struct Global{
    static let LOGGED_IN_USER                = AppStateManager.sharedInstance.loggedInUser
    static var APP_MANAGER                   = AppStateManager.sharedInstance
    static var APP_REALM                     = APP_MANAGER.realm
    static var APP_COLOR                     = UIColor(red:97/255, green:37/255, blue:42/255, alpha:1.0)
    static var APP_COLOR_LIGHT               = UIColor(red:245/255, green:248/255, blue:249/255, alpha:1.0)
    static var addToCart                   = ""
    static var changePassword              = ""
    static var deleteFromCart              = ""
    static var forgetPassword              = ""
    static var PushNotificationFirebase    = ""
    static var cartCount                   = 0
    static var searchCheck                 = false
    static var flagSignUp                  = false
    static var FCMTokenFirebase            = ""
}
//192.168.18.76
// home::: 192.168.0.102
// new::: http://e158e6e42590.ngrok.io//
//"http://ec2-18-216-136-42.us-east-2.compute.amazonaws.com:8443/
//"http://localhost:8081/"
/// http://ec2-18-216-136-42.us-east-2.compute.amazonaws.com:8443//
///http://e92b9ebf315f.ngrok.io



struct Constants {
    //MARK:- Base URL
    static let BaseURL                     = "https://www.thecloneconservatory.com/wp-json/"
    static let APP_DELEGATE                = UIApplication.shared.delegate as! AppDelegate
    static let UIWINDOW                    = UIApplication.shared.delegate!.window!
    static let UIAPPLICATION               = UIApplication.shared
    static let USER_DEFAULTS               = UserDefaults.standard
    static let DEFAULTS_USER_KEY           = "User"
    static var DeviceToken                 = "NoCertificates"
    static let serverDateFormat            = "yyyy-MM-dd'T'HH:mm:ss.SSS"//"yyyy-MM-dd HH:mm:ss""
    static let PAGINATION_PAGE_SIZE        = 100
    static var adminPhone                  = "123456"
    //MARK:- Notification observer names
    
    static let NotificationCount           = "NotificationCount"
    static let apiKey                      = "AIzaSyCDylplefNyWlLDoBL_n2VFjwlMWvq3sBg"
    static var accessToken                 = ""
    static var GoogleSignInClientID        = "821622832147-00elb5glohhc0njcl57bksf4b9kftomc.apps.googleusercontent.com"
    static var KeyboardHeight:CGFloat      = 0.0
    static var MAXIMUM_FILES_SIZE_ALLOWED_IN_MBs = 25
    static var apiMessage                  = ""
    static var orderQr                  = ""
}

