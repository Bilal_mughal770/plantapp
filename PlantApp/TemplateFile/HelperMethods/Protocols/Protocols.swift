//
//  Protocols.swift
//  PlantApp
//
//  Created by Muhammad Osama Afaque on 24/07/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import Foundation
import UIKit

protocol Identifiable {
    static var identifier: String { get }
}

extension UIResponder: Identifiable {
    static var identifier: String {
        return "\(self)"
    }
}
