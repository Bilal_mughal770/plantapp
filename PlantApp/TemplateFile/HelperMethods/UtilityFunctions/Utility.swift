import Foundation
import UIKit
import AVFoundation
import Toast_Swift

//// MARK: AppHelperUtility setup
@objc class Utility: NSObject{
    static let main = Utility()
    fileprivate override init() {}
    var alertType:String!
}

struct spinnerViewConfig {
    static let tag: Int = 98272
    static let color = UIColor.white
}


extension Utility{
    func roundAndFormatFloat(floatToReturn : Float, numDecimalPlaces: Int) -> String{
        
        let formattedNumber = String(format: "%.\(numDecimalPlaces)f", floatToReturn)
        return formattedNumber
        
    }
    func printFonts() {
        for familyName in UIFont.familyNames {
            print("\n-- \(familyName) \n")
            for fontName in UIFont.fontNames(forFamilyName: familyName) {
                print(fontName)
            }
        }
    }
    func topViewController(base: UIViewController? = (Constants.APP_DELEGATE).window?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
    func showAlert(title:String?, message:String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Strings.OK.text, style: .default) { _ in })
        Utility().topViewController()!.present(alert, animated: true){}
    }
    func resizeImage(image: UIImage,  targetSize: CGFloat) -> UIImage {
        
        guard (image.size.width > 1024 || image.size.height > 1024) else {
            return image;
        }
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newRect: CGRect = CGRect.zero;
        
        if(image.size.width > image.size.height) {
            newRect.size = CGSize(width: targetSize, height: targetSize * (image.size.height / image.size.width))
        } else {
            newRect.size = CGSize(width: targetSize * (image.size.width / image.size.height), height: targetSize)
        }
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newRect.size, false, 1.0)
        image.draw(in: newRect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    func thumbnailForVideoAtURL(url: URL) -> UIImage? {
        
        let asset = AVAsset(url: url)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImageGenerator.appliesPreferredTrackTransform=true
        
        var time = asset.duration
        time.value = min(time.value, 2)
        
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch {
            print("error")
            return nil
        }
    }
    func openiTunesURL(){
        let urlStr = "itms-apps://itunes.apple.com/app/apple-store/id375380948?mt=8"
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
            
        } else {
            UIApplication.shared.openURL(URL(string: urlStr)!)
        }
    }
    func openEmail(email: String) {
        if let url = URL(string: "mailto:\(email)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
                
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    func delay(delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    func converArabicToEnglishNumbers(_ numbers:String) -> String{
        let formatter: NumberFormatter = NumberFormatter()
        formatter.locale = Locale(identifier: "en")
        guard let final = formatter.number(from: numbers) else {return ""}
        return "\(final)"
    }
    func converEnglishToArabicNumbers(_ numbers:String) -> String{
        let formatter: NumberFormatter = NumberFormatter()
        formatter.locale = Locale(identifier: "ar")
        guard let final = formatter.number(from: numbers) else {return ""}
        return "\(final)"
    }
    
}
//MARK:- INDICATOR
extension Utility{
    static func showLoader() {
        //        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        //        let size = CGSize(width: 50, height: 50)
        //        let bgColor = UIColor.clear
        //        let activityData = ActivityData(size: size, message: "", messageFont: UIFont.systemFont(ofSize: 12), type: .ballRotateChase, color: Global.APP_COLOR, padding: 0, displayTimeThreshold: 0, minimumDisplayTime: 1, backgroundColor: bgColor, textColor: UIColor.black)
        
        //        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
    }
    
    static func hideLoader() {
        //        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        //        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
}
//MARK:- Make phone call
extension Utility{
    func makeCallTo(number:String){
        guard let number = URL(string: "tel://" + number) else { return }
        UIApplication.shared.open(number)
    }
}
// MARK: Alert related functions
extension Utility{
    func showAlert(message:String,title:String,controller:UIViewController){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.view.tintColor = Global.APP_COLOR
        alertController.addAction(UIAlertAction(title: Strings.OK.text, style: .default, handler: nil))
        controller.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertWithOptionalController(message:String,title:String,controller:UIViewController?) -> UIViewController?{
        if(controller == nil){
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: Strings.OK.text, style: .default, handler: nil))
            return alertController
        }
        else{
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: Strings.OK.text, style: .default, handler: nil))
            controller!.present(alertController, animated: true, completion: nil)
            return nil
        }
    }
    
    func showAlert(message: String, title: String, controller: UIViewController, usingCompletionHandler handler:@escaping (() -> Swift.Void))
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: Strings.OK.text, style: .default, handler: {
            
            (action) in
            
            handler()
        }
        ))
        controller.present(alertController, animated: true, completion: nil)
    }
    
    func showAlert(message:String,title:String,YES:String,NO:String,controller:UIViewController, completionHandler: @escaping (UIAlertAction?, UIAlertAction?) -> Void){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: YES, style: .default){ (alertActionYES) in
            completionHandler(alertActionYES, nil)
        })
        
        alertController.addAction(UIAlertAction(title: NO, style: .cancel){ (alertActionNO) in
            completionHandler(nil, alertActionNO)
        })
        
        controller.present(alertController, animated: true, completion: nil)
        
    }
    
    func showAlert(message:String,title:String,controller:UIViewController, completionHandler: @escaping (UIAlertAction?, UIAlertAction?) -> Void){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: Strings.YES.text, style: .default){ (alertActionYES) in
            completionHandler(alertActionYES, nil)
        })
        
        alertController.addAction(UIAlertAction(title: Strings.NO.text, style: .cancel){ (alertActionNO) in
            completionHandler(nil, alertActionNO)
        })
        
        controller.present(alertController, animated: true, completion: nil)
        
    }
    
    func showAlert(message:String,title:String){
        if var topController = UIApplication.shared.windows.first?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: Strings.OK.text, style: .default, handler: nil))
            Utility.main.topViewController()?.present(alertController, animated: true, completion: nil)
            
        }
    }
    
    func convertToArray(text: String) -> Array<Any>? {
        if let data = text.data(using: String.Encoding.utf8) {
            NSLog("Enter here")
            do {
                return try JSONSerialization.jsonObject(with: data, options: [.allowFragments, .mutableContainers, .mutableLeaves]) as? Array
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
        
    }
    
    func showActivityViewController(with sharingText: String)
    {
        let activityViewController = UIActivityViewController(
            activityItems: [sharingText],
            applicationActivities: nil)
        let topController = UIApplication.shared.windows.first?.rootViewController
        topController?.present(activityViewController, animated: true, completion: nil)
    }
    
    func showToast(message:String){
        Utility().topViewController()?.view.makeToast(message, duration: 2.0, position: .center, title: nil, image: nil, style: .init(), completion: nil)
    }
    
    func goToSettings(){
        let alertController = UIAlertController (title: "Settings", message: "Location permission is mendatory to give customer access to track his delivery.\nGo to Settings?", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        Utility.main.topViewController()?.present(alertController, animated: true, completion: nil)
    }
}
// MARK:- Utility Methods
extension Utility{
    static func stringDateFormatterCalendar(dateStr: String , dateFormat : String , formatteddate : String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = dateFormat
        let date = dateFormatter.date(from: dateStr)
        dateFormatter.dateFormat = formatteddate
        return dateFormatter.string(from: date ?? Date())
    }
    static func stringDateFormatter(dateStr: String , dateFormat : String , formatteddate : String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = dateFormat
        let date = dateFormatter.date(from: dateStr)
        dateFormatter.dateFormat = formatteddate
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        return dateFormatter.string(from: date ?? Date())
    }
    static func dateFormatter(date: Date,dateFormat:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: date)
    }
    static func dateFormatter_US(date: Date,dateFormat:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: date)
    }
    static func getDateFrom(dateString: String,dateFormat: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        guard let date = dateFormatter.date(from: dateString) else {return nil}
        return date
    }
    static func getCurrentDate() -> String {
        let dateFormat = "yyyy-MM-dd"
        let toDate = Date()
        let fromDate = Calendar.current.date(byAdding: .month, value: 0, to: toDate)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: fromDate ?? Date())
    }
    static func getOneMonthEarlier() -> String {
        let dateFormat = "yyyy-MM-dd"
        let toDate = Date()
        let fromDate = Calendar.current.date(byAdding: .month, value: -1, to: toDate)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: fromDate ?? Date())
    }
    static func showLanguageAlert(language:String, message:String,title:String,controller:UIViewController, completionHandler: @escaping (UIAlertAction?, UIAlertAction?) -> Void){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.view.tintColor = .black
        if language == "en"{
            alertController.addAction(UIAlertAction(title: "YES", style: .default){ (alertActionYES) in
                completionHandler(alertActionYES, nil)
            })
            alertController.addAction(UIAlertAction(title: "NO", style: .cancel){ (alertActionNO) in
                completionHandler(nil, alertActionNO)
            })
        }
        else{
            alertController.addAction(UIAlertAction(title: "نعم فعلا", style: .default){ (alertActionYES) in
                completionHandler(alertActionYES, nil)
            })
            alertController.addAction(UIAlertAction(title: "لا", style: .cancel){ (alertActionNO) in
                completionHandler(nil, alertActionNO)
            })
        }
        controller.present(alertController, animated: true, completion: nil)
    }
    static func getAgeFromDate(dateStr:String)->String{
        let dateFormat = "dd/MM/yyyy"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        let birthday = dateFormatter.date(from: dateStr) ?? Date()
        
        let now = Date()
        let calendar = Calendar.current
        
        let ageComponents = calendar.dateComponents([.year], from: birthday, to: now)
        let age = ageComponents.year ?? 0
        return "\(age)"
    }
    static func getAllCountries()->[String]{
        var arrCountries = [String]()
        for code in NSLocale.isoCountryCodes as [String] {
            let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
            let name = NSLocale(localeIdentifier: "en_UK").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
            arrCountries.append(name)
        }
        return arrCountries
    }
    static func emojiFlag(regionCode: String) -> String? {
        let code = regionCode.uppercased()
        
        guard Locale.isoRegionCodes.contains(code) else {
            return nil
        }
        
        var flagString = ""
        for s in code.unicodeScalars {
            guard let scalar = UnicodeScalar(127397 + s.value) else {
                continue
            }
            flagString.append(String(scalar))
        }
        return flagString
    }
//    static func validatePhoneNumber(number:String)->Bool{
//        let phoneNumberKit = PhoneNumberKit()
//        do {
//            let _ = try phoneNumberKit.parse(number).countryCode
//            return true
//        }
//        catch {
//            return false
//        }
//    }
    static func countryName(from countryCode: String) -> String {
        if let name = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: countryCode) {
            // Country name was found
            return name
        } else {
            // Country name cannot be found
            return countryCode
        }
    }
    static func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    static func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    
    static func dataSize(items: [Data]) -> Int{
        var MBsCount:Int64 = 0
        let bcf = ByteCountFormatter()
        bcf.allowedUnits = [.useMB]
        bcf.countStyle = .file
        for item in items{
            MBsCount += Int64(item.count)
        }
        return Int(MBsCount/1024)
    }

}


//MARK:- Table View Cell Animations
extension Utility{
    
    static func tableViewCellAnimationFaded(cell:UITableViewCell,indexPath:IndexPath){
        //before animation
        cell.alpha = 0
        //after animation
        UIView.animate(withDuration: 0.2, delay: 0.02 * Double(indexPath.row), animations: {
            cell.alpha = 1
        })
        
    }
    
    static func tableViewCellAnimationBounching(cell:UITableViewCell,indexPath:IndexPath){
        cell.transform = CGAffineTransform(scaleX: 1, y: 0)
        UIView.animate(withDuration: 1, delay: 0.05 * Double(indexPath.row), usingSpringWithDamping: 0.6, initialSpringVelocity: 0.01 , animations: {
            cell.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
        
    }
    
    
    static func tableViewCellAnimationFromFar(cell:UITableViewCell,indexPath:IndexPath){
        cell.transform = CGAffineTransform(scaleX: 0.35, y: 0.35)
        UIView.animate(withDuration: 0.5, delay: 0.035 * Double(indexPath.row), animations: {
            cell.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    static func tableViewCellAnimationInverted(cell:UITableViewCell,indexPath:IndexPath){
        cell.transform = CGAffineTransform(scaleX: 1, y: 0.25)
        UIView.animate(withDuration: 0.35, delay: 0.035 * Double(indexPath.row), animations: {
            cell.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
}

//MARK:- Loader shimmer



//MARK:- Custom RGB Value Function
extension Utility {
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}








