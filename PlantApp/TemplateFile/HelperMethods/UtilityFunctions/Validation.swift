import Foundation
import UIKit
class Validation {
    
    static func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    static func isValidateLength(_ value:String,length:Int) -> Bool {
        return value.count >= length ? true : false
    }
    
    static func isValidPassword(_ value:String) -> Bool {
        let pwdRegEx = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$"
        
        let pwdTest = NSPredicate(format:"SELF MATCHES %@", pwdRegEx)
        return pwdTest.evaluate(with: value)
    }
    
    static func isValidName(_ value: String) -> Bool {
        if value.count >= 2 {
            return true
        }
        return false
    }
    
    
    static  func isValidNameWithSpecialCharacters(str: String) -> Bool
    {
        do
        {
            let regex = try NSRegularExpression(pattern: "^[a-zA-Z\\]{2,18}$", options: .caseInsensitive)
            if regex.matches(in: str, options: [], range: NSMakeRange(0, str.count)).count > 0 {return true}
        }
        catch {}
        return false
    }
    
    
    static func isValidWithOutSpecialCharactersAndNumbers(_ value:String) -> Bool {
        let validString = CharacterSet(charactersIn: "!@#$%^&*()_+{}[]|\"<>,.~`/:;?-=\\¥'£•¢1234567890")
        if value.rangeOfCharacter(from: validString) != nil {
            return false
        }
        return true
    }
    
    static func isValidWithOutSpecialCharacters(_ value:String) -> Bool {
        let validString = CharacterSet(charactersIn: "!@#$%^&*()_+{}[]|\"<>,.~`/:;?-=\\¥'£•¢")
        if value.rangeOfCharacter(from: validString) != nil {
            return false
        }
        return true
    }

    
    
    static func isValidPhoneNumber(_ value: String) -> Bool {
        guard let _ = Int(value),value.count > 7 else{return false}
        return true
    }
    
    static func isConfirmPasswordIsEqualToPassword(password: String, confirm: String) -> Bool {
        if(password == confirm){
            return true
        }
        return false
    }
    
    static func validateStringLength(_ text: String) -> Bool {
        let trimmed = text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return !trimmed.isEmpty
    }
    
    static func validateDouble(_ text: String) -> Bool {
        if let _ = Double(text){
            return true
        }
        else{
            return false
        }
    }
    
    static func provideAllFields(name: String,email: String,phoneNumber: String,zipCode: String,stateId: String,cityId: String,password: String,confirmPassword: String,address: String) -> Bool{
        let arrayCheck = [name,email,phoneNumber,zipCode,stateId,cityId,password,confirmPassword,address] as [Any]
        for text in arrayCheck {
            let trimmed = (text as AnyObject).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            if trimmed.isEmpty{
                return false
            }
        }
        return true
    }
    
}
