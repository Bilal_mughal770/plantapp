//
//  AppDelegate.swift
//  PlantApp
//
//  Created by Nayyer Ali on 11/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import UIKit

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    static let shared: AppDelegate = UIApplication.shared.delegate as! AppDelegate


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.observersForTheme()
        self.changeRootViewController()
       return true
    }

}


extension AppDelegate {
    
    func observersForTheme(){
        
        if UserDefaults.standard.bool(forKey: isDarkModeOn){
            window?.overrideUserInterfaceStyle = .dark
        } else {
            window?.overrideUserInterfaceStyle = .light
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(changeToDarkMode), name: darkThemeOn, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeToLightMode), name: darkThemeOff, object: nil)
    }
    
    @objc func changeToDarkMode(){
        window?.overrideUserInterfaceStyle = .dark
    }
    
    @objc func changeToLightMode(){
        window?.overrideUserInterfaceStyle = .light
    }
}

//MARK:- Application Flow
extension AppDelegate {
    func changeRootViewController(){
        
            if !AppStateManager.sharedInstance.isUserLoggedIn(){
                self.showLogin()
            }
            else{
                self.showHome()
            }
        }
    
    func showLogin(){
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let navigationController = BaseNavigationController(rootViewController: storyBoard.instantiateViewController(withIdentifier: SignInController.identifier))
        self.window?.rootViewController = navigationController
        navigationController.navigationBar.isHidden = true
    }
    
    func showHome(){
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let navigationController = BaseNavigationController(rootViewController: storyBoard.instantiateViewController(withIdentifier: HomeController.identifier))
        self.window?.rootViewController = navigationController
        navigationController.navigationBar.isHidden = true
    }

}

