//
//  StrainsList.swift
//  PlantApp
//
//  Created by Nayyer Ali on 30/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import Foundation

struct StrainsList{
    
    var productName: String
    var productURL: Any
    var productImageURL: String?
    var productID:Int
    
    enum CodingKeys: String {
        
        case productName = "Product-Name"
        case productURL = "Product-Url"
        case productImageURL = "Product-Image-Url"
        case productID = "Product-id"
    }
    
    init(productName:String, productURL:Any, productImageURL:String,productID:Int ){

        self.productName = productName
        self.productURL = productURL
        self.productImageURL = productImageURL
        self.productID = productID
    }
}
