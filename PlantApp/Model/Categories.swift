import Foundation

struct Categories : Codable {
    
	let id : Int?
	let count : Int?
	let description : String?
	let link : String?
	let name : String?
	let slug : String?
	let taxonomy : String?
	let parent : Int?
	let meta : [String]?
	let yoast_head : String?
//	let _links : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case count = "count"
		case description = "description"
		case link = "link"
		case name = "name"
		case slug = "slug"
		case taxonomy = "taxonomy"
		case parent = "parent"
		case meta = "meta"
		case yoast_head = "yoast_head"
//		case _links = "_links"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		count = try values.decodeIfPresent(Int.self, forKey: .count)
		description = try values.decodeIfPresent(String.self, forKey: .description)
		link = try values.decodeIfPresent(String.self, forKey: .link)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		slug = try values.decodeIfPresent(String.self, forKey: .slug)
		taxonomy = try values.decodeIfPresent(String.self, forKey: .taxonomy)
		parent = try values.decodeIfPresent(Int.self, forKey: .parent)
		meta = try values.decodeIfPresent([String].self, forKey: .meta)
		yoast_head = try values.decodeIfPresent(String.self, forKey: .yoast_head)
//		_links = try values.decodeIfPresent(String.self, forKey: ._links)
	}
}
