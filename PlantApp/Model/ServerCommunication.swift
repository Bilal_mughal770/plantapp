//
//  ServerCommunication.swift
//  PlantApp
//
//  Created by Nayyer Ali on 29/05/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class ServerCommunication{
    
    static var shared = ServerCommunication()
    private var baseUrl = "https://www.thecloneconservatory.com/wp-json/"
    private var loginURL = "customer/login"
    private var categoriesURL = "wp/v2/categories"
    private var strainsListURL = "strain/strain-list"
    private var productsByNightTimeUsage = "products/night-time-usage"
    private var productsByCount = "product/product-list-count"
    private var productsByPopulerThisWeek = "popular/products"
    private var videosListForYou = "wp/v2/video"
    
    func videosListNew( completion:@escaping(_ status:Bool, _ message:String, _ data:[String:Any]?)-> Void){
        
        let requestURL = baseUrl + videosListForYou
        
        let _ = Alamofire.request(requestURL, method: .get).responseJSON { (response) in
            
            switch response.result{
            case .success(_):
                
                if let statusCode = response.response?.statusCode{
                    
                    if statusCode >= 200 && statusCode < 304 {
//                        print("Test")
//                        print(response.value)
//                        guard let responseData = response.value as? [String: Any] else {return}
//                        var test: [String:Any] = []
//                        var index = 0
//                        for data in responseData{
//                            index += 1
//                            test.append(["\(index)": data])
//                        }
//                        print("Test")
//                        print(responseData)
//                        completion(true, "Got Data", responseData)
                    } else {
                        let responseResult = response.result
                        print("Error in loginRequest status code === \(statusCode)")
                        print("Error in loginRequest response result === \(responseResult)")
                    }
                }
            case .failure(_):
                let err = response.error
                if err != nil{
                    let statusCode = response.response?.statusCode
                    print("Found error with status code === \(String(describing: statusCode)) & error == \(err!.localizedDescription))")
                    
                    completion(false, "Invalid email/ password entered.",nil)
                }
            }
        }
    }
    
    func loginRequest(loginDetails:[String:Any], completion:@escaping(_ status:Bool, _ message:String)-> Void){
        
        let requestURL = baseUrl + loginURL
        
        let _ = Alamofire.request(requestURL, method: .post, parameters: loginDetails, encoding: JSONEncoding.default).responseJSON { (response) in
            
            switch response.result{
            case .success(_):
                if let statusCode = response.response?.statusCode{
                    if statusCode >= 200 && statusCode < 304 {
                        if let json = response.value{
                            if json is NSDictionary {
                                //                                let test = json as! NSDictionary
                                completion(true, "User Logged In Successfully")
                            }
                        }
                    } else {
                        let responseResult = response.result
                        print("Error in loginRequest status code === \(statusCode)")
                        print("Error in loginRequest response result === \(responseResult)")
                    }
                }
            case .failure(_):
                let err = response.error
                if err != nil{
                    let statusCode = response.response?.statusCode
                    print("Found error with status code === \(String(describing: statusCode)) & error == \(err!.localizedDescription))")
                    
                    completion(false, "Invalid email/ password entered.")
                }
            }
        }
    }
    
    func categoriesRequest(bodyDetails:[String:Any], completion:@escaping(_ status:Bool, _ message:String, _ categories:[Categories]?)-> Void){
        
        let requestURL = baseUrl + categoriesURL
        
        let _ = Alamofire.request(requestURL, method: .get).responseJSON { (response) in
            
            guard let itemsData = response.data else {
                completion(false, "error", nil)
                return
            }
            do {
                let decoder = JSONDecoder()
                let items = try decoder.decode([Categories].self, from: itemsData)
                DispatchQueue.main.async {
                    completion(true, "data", items)
                }
            } catch {
                completion(false, "error22", nil)
            }
        }
    }
    
    func strainsListRequest( completion:@escaping(_ status:Bool, _ message:String, _ data:[StrainsList]?)-> Void){
        
        let requestURL = baseUrl + strainsListURL
        
        let _ = Alamofire.request(requestURL, method: .get).responseJSON { (response) in
            print(response)
            
            switch response.result{
            case .success(_):
                
                if let statusCode = response.response?.statusCode{
                    
                    if statusCode >= 200 && statusCode < 304 {
                        
                        var strains = [StrainsList]()
                        
                        if let response = response.value as? [String:Any] {
                            //                            let arrResponse = [[String:Any]]()
                            for (_, value) in response {
                                
                                guard let strain = value as? [String : Any] else {return}
                                
                                let p_name = strain["Product-Name"] as! String
                                let p_id = strain["Product-id"] as! Int
                                let p_image_url = strain["Product-Image-Url"] as? String
                                let p_url = strain["Product-Url"] as Any
                                
                                let data = StrainsList(productName: p_name, productURL: p_url, productImageURL: p_image_url ?? "", productID: p_id)
                                strains.append(data)
                            }
                            print("Data Aya KIa === \(strains)")
                            DispatchQueue.main.async {
                                completion(true, "Dta agya", strains)
                                
                            }
                        } else {
                            print("Bhand")
                        }
                    } else {
                        let responseResult = response.result
                        print("Error in loginRequest status code === \(statusCode)")
                        print("Error in loginRequest response result === \(responseResult)")
                    }
                }
            case .failure(_):
                let err = response.error
                if err != nil{
                    let statusCode = response.response?.statusCode
                    print("Found error with status code === \(String(describing: statusCode)) & error == \(err!.localizedDescription))")
                    
                    completion(false, "Invalid email/ password entered.",nil)
                }
            }
        }
    }
    
    func productsByNightTimeUsage( completion:@escaping(_ status:Bool, _ message:String, _ data:[ProductsByNightTime]?)-> Void){
        
        let requestURL = baseUrl + productsByNightTimeUsage
        
        let _ = Alamofire.request(requestURL, method: .get).responseJSON { (response) in
            
            switch response.result{
            case .success(_):
                
                if let statusCode = response.response?.statusCode{
                    
                    if statusCode >= 200 && statusCode < 304 {
                        
                        var products = [ProductsByNightTime]()
                        
                        if let response = response.value as? [String:Any] {
                            
                            for (_, value) in response {
                                
                                guard let product = value as? [String : Any] else {return}
                                
                                let p_name = product["Product-Name"] as! String
                                let p_id = product["Product-id"] as! Int
                                let p_image_url = product["Product-Image-Url"] as? String
                                let p_url = product["Product-id"] as! Int
                                
                                let data = ProductsByNightTime(productName: p_name, productURL: p_url, productImageURL: p_image_url ?? "", productID: p_id)
                                products.append(data)
                            }
                            DispatchQueue.main.async {
                                completion(true, "Data agya", products)
                            }
                        } else {
                            print("Bhand")
                        }
                    } else {
                        let responseResult = response.result
                        print("Error in loginRequest status code === \(statusCode)")
                        print("Error in loginRequest response result === \(responseResult)")
                    }
                }
            case .failure(_):
                let err = response.error
                if err != nil{
                    let statusCode = response.response?.statusCode
                    print("Found error with status code === \(String(describing: statusCode)) & error == \(err!.localizedDescription))")
                    
                    completion(false, "Invalid email/ password entered.",nil)
                }
            }
        }
    }
    
    func productsByCount( completion:@escaping(_ status:Bool, _ message:String, _ data:[ProductsByCount]?)-> Void){
        
        let requestURL = baseUrl + productsByCount
        
        let _ = Alamofire.request(requestURL, method: .get).responseJSON { (response) in
            
            switch response.result{
            case .success(_):
                
                if let statusCode = response.response?.statusCode{
                    
                    if statusCode >= 200 && statusCode < 304 {
                        
                        var products = [ProductsByCount]()
                        
                        if let response = response.value as? [String:Any] {
                            
                            for (_, value) in response {
                                
                                guard let product = value as? [String : Any] else {return}
                                
                                let p_name = product["category-Name"] as! String
                                let total_p = product["Total-Product"] as! Int
                                let p_image_url = product["category-Image-Url"] as? String
                                let p_id = product["Category-id"] as! Int
                                
                                let data = ProductsByCount(categoryName: p_name, totalProduct: total_p, categoryImageURL: p_image_url ?? "", categoryId: p_id)
                                products.append(data)
                            }
                            DispatchQueue.main.async {
                                completion(true, "Data agya", products)
                            }
                        } else {
                            print("Bhand")
                        }
                    } else {
                        let responseResult = response.result
                        print("Error in loginRequest status code === \(statusCode)")
                        print("Error in loginRequest response result === \(responseResult)")
                    }
                }
            case .failure(_):
                let err = response.error
                if err != nil{
                    let statusCode = response.response?.statusCode
                    print("Found error with status code === \(String(describing: statusCode)) & error == \(err!.localizedDescription))")
                    
                    completion(false, "Invalid email/ password entered.",nil)
                }
            }
        }
    }
    
    func populerProducts( completion:@escaping(_ status:Bool, _ message:String, _ data:[ProductsByPopuler]?)-> Void){
        
        let requestURL = baseUrl + productsByPopulerThisWeek
        
        let _ = Alamofire.request(requestURL, method: .get).responseJSON { (response) in
            
            switch response.result{
            case .success(_):
                
                if let statusCode = response.response?.statusCode{
                    
                    if statusCode >= 200 && statusCode < 304 {
                        
                        var products = [ProductsByPopuler]()
                        
                        if let response = response.value as? [String:Any] {
                            
                            for (_, value) in response {
                                
                                guard let product = value as? [String : Any] else {return}
                                
                                let p_name = product["Product-Name"] as! String
                                let data = ProductsByPopuler(productName: p_name)
                                products.append(data)
                            }
                            DispatchQueue.main.async {
                                completion(true, "Data agya", products)
                            }
                        } else {
                            print("Bhand")
                        }
                    } else {
                        let responseResult = response.result
                        print("Error in loginRequest status code === \(statusCode)")
                        print("Error in loginRequest response result === \(responseResult)")
                    }
                }
            case .failure(_):
                let err = response.error
                if err != nil{
                    let statusCode = response.response?.statusCode
                    print("Found error with status code === \(String(describing: statusCode)) & error == \(err!.localizedDescription))")
                    
                    completion(false, "Invalid email/ password entered.",nil)
                }
            }
        }
    }
}
