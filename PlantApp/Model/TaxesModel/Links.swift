//
//  Links.swift
//
//  Created by Hamza Hasan on 25/07/2021
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

//public class Links: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
//  private struct SerializationKeys {
//    static let selfTax: String = "self"
//    static let collection = "collection"
//  }
//
//  // MARK: Properties
//  var selfTax = List<SelfTax>()
//  var collection = List<Collection>()
//
//  // MARK: ObjectMapper Initializers
//  /// Map a JSON object to this class using ObjectMapper.
//  ///
//  /// - parameter map: A mapping from ObjectMapper.
//
//  required convenience public init?(map : Map){
//    self.init()
//  }
//
////  override class func primaryKey() -> String? {
////    return "id"
////  }
//
//  /// Map a JSON object to this class using ObjectMapper.
//  ///
//  /// - parameter map: A mapping from ObjectMapper.
//  public func mapping(map: Map) {
//    selfTax <- (map[SerializationKeys.selfTax], ListTransform<SelfTax>())
//    collection <- (map[SerializationKeys.collection], ListTransform<Collection>())
//  }


//}
