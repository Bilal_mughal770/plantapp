//
//  TaxesModel.swift
//
//  Created by Hamza Hasan on 25/07/2021
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class TaxesModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let state = "state"
    static let compound = "compound"
    static let city = "city"
    static let name = "name"
    static let classProperty = "class"
    static let postcode = "postcode"
    static let links = "_links"
    static let priority = "priority"
    static let rate = "rate"
    static let id = "id"
    static let shipping = "shipping"
    static let order = "order"
    static let country = "country"
  }

  // MARK: Properties
    @objc dynamic var state: String? = ""
    @objc dynamic var compound = false
    @objc dynamic var city: String? = ""
    @objc dynamic var name: String? = ""
    @objc dynamic var classProperty: String? = ""
    @objc dynamic var postcode: String? = ""
//    @objc dynamic var links: Links?
    @objc dynamic var priority = 0
    @objc dynamic var rate: String? = ""
    @objc dynamic var id = 0
    @objc dynamic var shipping = false
    @objc dynamic var order = 0
    @objc dynamic var country: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    public override class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    state <- map[SerializationKeys.state]
    compound <- map[SerializationKeys.compound]
    city <- map[SerializationKeys.city]
    name <- map[SerializationKeys.name]
    classProperty <- map[SerializationKeys.classProperty]
    postcode <- map[SerializationKeys.postcode]
//    links <- map[SerializationKeys.links]
    priority <- map[SerializationKeys.priority]
    rate <- map[SerializationKeys.rate]
    id <- map[SerializationKeys.id]
    shipping <- map[SerializationKeys.shipping]
    order <- map[SerializationKeys.order]
    country <- map[SerializationKeys.country]
  }


}
