//
//  StrainModel.swift
//
//  Created by Hamza Hasan on 24/07/2021
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class StrainModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let productId = "Product-id"
    static let productUrl = "Product-Url"
    static let productName = "Product-Name"
    static let productImageUrl = "Product-Image-Url"
  }

  // MARK: Properties
 @objc dynamic var productId = 0
 @objc dynamic var productUrl: String? = ""
 @objc dynamic var productName: String? = ""
 @objc dynamic var productImageUrl: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    public override class func primaryKey() -> String? {
    return "productId"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    productId <- map[SerializationKeys.productId]
    productUrl <- map[SerializationKeys.productUrl]
    productName <- map[SerializationKeys.productName]
    productImageUrl <- map[SerializationKeys.productImageUrl]
  }


}
