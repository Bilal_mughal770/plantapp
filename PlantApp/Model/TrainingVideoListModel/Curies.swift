//
//  Curies.swift
//
//  Created by Hamza Hasan on 25/07/2021
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class Curies: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let name = "name"
    static let templated = "templated"
    static let href = "href"
  }

  // MARK: Properties
    @objc  dynamic var name: String? = ""
    @objc  dynamic var templated = false
    @objc  dynamic var href: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

  required convenience public init?(map : Map){
    self.init()
  }

//  override class func primaryKey() -> String? {
//    return "id"
//  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    name <- map[SerializationKeys.name]
    templated <- map[SerializationKeys.templated]
    href <- map[SerializationKeys.href]
  }


}
