//
//  Excerpt.swift
//
//  Created by Hamza Hasan on 25/07/2021
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class Excerpt: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let rendered = "rendered"
    static let protected = "protected"
  }

  // MARK: Properties
    @objc  dynamic var rendered: String? = ""
    @objc  dynamic var protected = false

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

  required convenience public init?(map : Map){
    self.init()
  }

//  override class func primaryKey() -> String? {
//    return "id"
//  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    rendered <- map[SerializationKeys.rendered]
    protected <- map[SerializationKeys.protected]
  }


}
