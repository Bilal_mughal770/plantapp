//
//  TrainingVideoListModel.swift
//
//  Created by Hamza Hasan on 25/07/2021
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class TrainingVideoListModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let meta = "meta"
    static let links = "_links"
    static let yoastHead = "yoast_head"
    static let date = "date"
    static let guid = "guid"
    static let menuOrder = "menu_order"
    static let type = "type"
    static let content = "content"
    static let status = "status"
    static let modified = "modified"
    static let link = "link"
    static let slug = "slug"
    static let modifiedGmt = "modified_gmt"
    static let commentStatus = "comment_status"
    static let id = "id"
    static let featuredMedia = "featured_media"
    static let videoCategory = "video_category"
    static let title = "title"
    static let excerpt = "excerpt"
    static let template = "template"
    static let pingStatus = "ping_status"
    static let dateGmt = "date_gmt"
  }

  // MARK: Properties
    @objc dynamic var meta: MetaTrainingVideoList?
    @objc dynamic var links: LinksTrainingVideoList?
    @objc dynamic var yoastHead: String? = ""
    @objc dynamic var date: String? = ""
    @objc dynamic var guid: Guid?
    @objc dynamic var menuOrder = 0
    @objc dynamic var type: String? = ""
    @objc dynamic var content: Content?
    @objc dynamic var status: String? = ""
    @objc dynamic var modified: String? = ""
    @objc dynamic var link: String? = ""
    @objc dynamic var slug: String? = ""
    @objc dynamic var modifiedGmt: String? = ""
    @objc dynamic var commentStatus: String? = ""
    @objc dynamic var id = 0
    @objc dynamic var featuredMedia = 0
    var videoCategory = List<Int>()
    @objc dynamic var title: TitleTrainingVideoList?
    @objc dynamic var excerpt: Excerpt?
    @objc dynamic var template: String? = ""
    @objc dynamic var pingStatus: String? = ""
    @objc dynamic var dateGmt: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

  required convenience public init?(map : Map){
    self.init()
  }

    public override class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    meta <- map[SerializationKeys.meta]
    links <- map[SerializationKeys.links]
    yoastHead <- map[SerializationKeys.yoastHead]
    date <- map[SerializationKeys.date]
    guid <- map[SerializationKeys.guid]
    menuOrder <- map[SerializationKeys.menuOrder]
    type <- map[SerializationKeys.type]
    content <- map[SerializationKeys.content]
    status <- map[SerializationKeys.status]
    modified <- map[SerializationKeys.modified]
    link <- map[SerializationKeys.link]
    slug <- map[SerializationKeys.slug]
    modifiedGmt <- map[SerializationKeys.modifiedGmt]
    commentStatus <- map[SerializationKeys.commentStatus]
    id <- map[SerializationKeys.id]
    featuredMedia <- map[SerializationKeys.featuredMedia]
    videoCategory <- map[SerializationKeys.videoCategory]
    title <- map[SerializationKeys.title]
    excerpt <- map[SerializationKeys.excerpt]
    template <- map[SerializationKeys.template]
    pingStatus <- map[SerializationKeys.pingStatus]
    dateGmt <- map[SerializationKeys.dateGmt]
  }


}
