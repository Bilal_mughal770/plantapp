//
//  Meta.swift
//
//  Created by Hamza Hasan on 25/07/2021
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class MetaTrainingVideoList: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let enterWhatBrand = "enter_what_brand"
    static let areYouUsingAnyNutrients = "are_you_using_any_nutrients"
    static let whatGrowMediumAreYouUsing = "what_grow_medium_are_you_using"
    static let olderlowerLeavesAffected = "olderlower_leaves_affected"
    static let doYouPhBalanceYourWater = "do_you_ph_balance_your_water"
    static let isYourGrowInsideOrOutside = "is_your_grow_inside_or_outside"
    static let yourNutrientCycle = "your_nutrient_cycle"
    static let videoUrl = "video_url"
    static let whatPointInTheGrowCycleAreYou = "what_point_in_the_grow_cycle_are_you"
    static let growingPointDiesTerminalBud = "growing_point_dies_terminal_bud"
    static let whatIsTheSpaceOfYourGrowAreaSqFt = "what_is_the_space_of_your_grow_area_sq_ft"
    static let spayEmail = "spay_email"
    static let howOftenDoYouWaterPlant = "how_often_do_you_water_plant"
    static let whatKindOfLightsAreYouUsing = "what_kind_of_lights_are_you_using"
  }

  // MARK: Properties
    @objc dynamic var enterWhatBrand: String? = ""
    @objc dynamic var areYouUsingAnyNutrients: String? = ""
    @objc dynamic var whatGrowMediumAreYouUsing: String? = ""
    @objc dynamic var olderlowerLeavesAffected: String? = ""
    @objc dynamic var doYouPhBalanceYourWater: String? = ""
    @objc dynamic var isYourGrowInsideOrOutside: String? = ""
    @objc dynamic var yourNutrientCycle: String? = ""
    @objc dynamic var videoUrl: String? = ""
    @objc dynamic var whatPointInTheGrowCycleAreYou: String? = ""
    @objc dynamic var growingPointDiesTerminalBud: String? = ""
    @objc dynamic var whatIsTheSpaceOfYourGrowAreaSqFt: String? = ""
    @objc dynamic var spayEmail: String? = ""
    @objc dynamic var howOftenDoYouWaterPlant: String? = ""
    @objc dynamic var whatKindOfLightsAreYouUsing: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

  required convenience public init?(map : Map){
    self.init()
  }

//  override class func primaryKey() -> String? {
//    return "id"
//  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    enterWhatBrand <- map[SerializationKeys.enterWhatBrand]
    areYouUsingAnyNutrients <- map[SerializationKeys.areYouUsingAnyNutrients]
    whatGrowMediumAreYouUsing <- map[SerializationKeys.whatGrowMediumAreYouUsing]
    olderlowerLeavesAffected <- map[SerializationKeys.olderlowerLeavesAffected]
    doYouPhBalanceYourWater <- map[SerializationKeys.doYouPhBalanceYourWater]
    isYourGrowInsideOrOutside <- map[SerializationKeys.isYourGrowInsideOrOutside]
    yourNutrientCycle <- map[SerializationKeys.yourNutrientCycle]
    videoUrl <- map[SerializationKeys.videoUrl]
    whatPointInTheGrowCycleAreYou <- map[SerializationKeys.whatPointInTheGrowCycleAreYou]
    growingPointDiesTerminalBud <- map[SerializationKeys.growingPointDiesTerminalBud]
    whatIsTheSpaceOfYourGrowAreaSqFt <- map[SerializationKeys.whatIsTheSpaceOfYourGrowAreaSqFt]
    spayEmail <- map[SerializationKeys.spayEmail]
    howOftenDoYouWaterPlant <- map[SerializationKeys.howOftenDoYouWaterPlant]
    whatKindOfLightsAreYouUsing <- map[SerializationKeys.whatKindOfLightsAreYouUsing]
  }


}
