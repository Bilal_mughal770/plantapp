//
//  Links.swift
//
//  Created by Hamza Hasan on 25/07/2021
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class LinksTrainingVideoList: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let replies = "replies"
    static let selfLinks = "self"
    static let wpattachment = "wp:attachment"
    static let wpfeaturedmedia = "wp:featuredmedia"
    static let about = "about"
    static let wpterm = "wp:term"
    static let collection = "collection"
    static let curies = "curies"
  }

  // MARK: Properties
  var replies = List<Replies>()
  var selfLinks = List<SelfTrainingVideoList>()
  var wpattachment = List<Wpattachment>()
  var wpfeaturedmedia = List<Wpfeaturedmedia>()
  var about = List<About>()
  var wpterm = List<Wpterm>()
  var collection = List<CollectionTrainingVideoList>()
  var curies = List<Curies>()

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

  required convenience public init?(map : Map){
    self.init()
  }

//  override class func primaryKey() -> String? {
//    return "id"
//  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    replies <- (map[SerializationKeys.replies], ListTransform<Replies>())
    selfLinks <- (map[SerializationKeys.selfLinks], ListTransform<SelfTrainingVideoList>())
    wpattachment <- (map[SerializationKeys.wpattachment], ListTransform<Wpattachment>())
    wpfeaturedmedia <- (map[SerializationKeys.wpfeaturedmedia], ListTransform<Wpfeaturedmedia>())
    about <- (map[SerializationKeys.about], ListTransform<About>())
    wpterm <- (map[SerializationKeys.wpterm], ListTransform<Wpterm>())
    collection <- (map[SerializationKeys.collection], ListTransform<CollectionTrainingVideoList>())
    curies <- (map[SerializationKeys.curies], ListTransform<Curies>())
  }


}
