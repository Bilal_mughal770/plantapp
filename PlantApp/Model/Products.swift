//
//  Products.swift
//  PlantApp
//
//  Created by Nayyer Ali on 20/06/2021.
//  Copyright © 2021 Nayyer Ali. All rights reserved.
//

import Foundation

struct ProductsByNightTime{
    
    var productName: String
    var productURL: Any
    var productImageURL: String?
    var productID:Int
    
    enum CodingKeys: String {
        
        case productName = "Product-Name"
        case productURL = "Product-Url"
        case productImageURL = "Product-Image-Url"
        case productID = "Product-id"
    }
    
    init(productName:String, productURL:Any, productImageURL:String,productID:Int ){

        self.productName = productName
        self.productURL = productURL
        self.productImageURL = productImageURL
        self.productID = productID
    }
}

struct ProductsByCount{
    
    var categoryName: String
    var totalProduct: Int
    var categoryImageURL: String?
    var categoryId:Int
    
    enum CodingKeys: String {
        
        case categoryName = "category-Name"
        case totalProduct = "Total-Product"
        case categoryImageURL = "category-Image-Url"
        case categoryId = "Category-id"
    }
    
    init(categoryName:String, totalProduct:Int, categoryImageURL:String,categoryId:Int ){

        self.categoryName = categoryName
        self.totalProduct = totalProduct
        self.categoryImageURL = categoryImageURL
        self.categoryId = categoryId
    }
}

struct ProductsByPopuler{
    
    var productName: String
    
    enum CodingKeys: String {
        
        case productName = "Product-Name"
    }
    
    init(productName:String){

        self.productName = productName
    }
}
