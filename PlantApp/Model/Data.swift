//
//  Data.swift
//
//  Created by Hamza Hasan on 24/07/2021
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class Data: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let iD = "ID"
    static let userRegistered = "user_registered"
    static let userLogin = "user_login"
    static let userActivationKey = "user_activation_key"
    static let userEmail = "user_email"
    static let displayName = "display_name"
    static let userNicename = "user_nicename"
    static let userStatus = "user_status"
    static let userUrl = "user_url"
    static let userPass = "user_pass"
  }

  // MARK: Properties
  dynamic var iD: String? = ""
  dynamic var userRegistered: String? = ""
  dynamic var userLogin: String? = ""
  dynamic var userActivationKey: String? = ""
  dynamic var userEmail: String? = ""
  dynamic var displayName: String? = ""
  dynamic var userNicename: String? = ""
  dynamic var userStatus: String? = ""
  dynamic var userUrl: String? = ""
  dynamic var userPass: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

  required convenience init?(map : Map){
    self.init()
  }

  override class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    iD <- map[SerializationKeys.iD]
    userRegistered <- map[SerializationKeys.userRegistered]
    userLogin <- map[SerializationKeys.userLogin]
    userActivationKey <- map[SerializationKeys.userActivationKey]
    userEmail <- map[SerializationKeys.userEmail]
    displayName <- map[SerializationKeys.displayName]
    userNicename <- map[SerializationKeys.userNicename]
    userStatus <- map[SerializationKeys.userStatus]
    userUrl <- map[SerializationKeys.userUrl]
    userPass <- map[SerializationKeys.userPass]
  }


}
