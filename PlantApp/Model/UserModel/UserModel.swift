//
//  UserModel.swift
//
//  Created by Hamza Hasan on 24/07/2021
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class UserModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let ID = "ID"
    static let capKey = "cap_key"
    static let data = "data"
    static let caps = "caps"
    static let roles = "roles"
    static let allcaps = "allcaps"
  }

  // MARK: Properties
 @objc dynamic var ID = 0
 @objc dynamic var capKey: String? = ""
 @objc dynamic var data: UserData?
 @objc dynamic var caps: Caps?
 var roles = List<String>()
 @objc dynamic var allcaps: Allcaps?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    public override class func primaryKey() -> String? {
    return "ID"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    ID <- map[SerializationKeys.ID]
    capKey <- map[SerializationKeys.capKey]
    data <- map[SerializationKeys.data]
    caps <- map[SerializationKeys.caps]
    roles <- map[SerializationKeys.roles]
    allcaps <- map[SerializationKeys.allcaps]
  }


}
