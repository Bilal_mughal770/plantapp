//
//  Data.swift
//
//  Created by Hamza Hasan on 24/07/2021
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class UserData: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let ID = "ID"
    static let userRegistered = "user_registered"
    static let userActivationKey = "user_activation_key"
    static let userEmail = "user_email"
    static let displayName = "display_name"
    static let userLogin = "user_login"
    static let userNicename = "user_nicename"
    static let userStatus = "user_status"
    static let userUrl = "user_url"
    static let userPass = "user_pass"
  }

  // MARK: Properties
 @objc dynamic var ID: String? = ""
 @objc dynamic var userRegistered: String? = ""
 @objc dynamic var userActivationKey: String? = ""
 @objc dynamic var userEmail: String? = ""
 @objc dynamic var displayName: String? = ""
 @objc dynamic var userLogin: String? = ""
 @objc dynamic var userNicename: String? = ""
 @objc dynamic var userStatus: String? = ""
 @objc dynamic var userUrl: String? = ""
 @objc dynamic var userPass: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    public override class func primaryKey() -> String? {
    return "ID"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    ID <- map[SerializationKeys.ID]
    userRegistered <- map[SerializationKeys.userRegistered]
    userActivationKey <- map[SerializationKeys.userActivationKey]
    userEmail <- map[SerializationKeys.userEmail]
    displayName <- map[SerializationKeys.displayName]
    userLogin <- map[SerializationKeys.userLogin]
    userNicename <- map[SerializationKeys.userNicename]
    userStatus <- map[SerializationKeys.userStatus]
    userUrl <- map[SerializationKeys.userUrl]
    userPass <- map[SerializationKeys.userPass]
  }


}
