//
//  DiscoverMenuModel.swift
//
//  Created by Hamza Hasan on 25/07/2021
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class DiscoverMenuModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let menuItem = "Menu-item"
    static let url = "Url"
    static let menuId = "Menu-id"
  }

  // MARK: Properties
 @objc dynamic var menuItem: String? = ""
 @objc dynamic var url: String? = ""
 @objc dynamic var menuId = 0

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    public override class func primaryKey() -> String? {
    return "menuId"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    menuItem <- map[SerializationKeys.menuItem]
    url <- map[SerializationKeys.url]
    menuId <- map[SerializationKeys.menuId]
  }


}
